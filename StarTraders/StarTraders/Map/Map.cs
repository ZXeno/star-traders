﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace StarTraders
{
    public class Map
    {
        public static Map Instance { get; private set; }
        public bool IsLoaded { get; private set; }

        private Random _rnd;

        private Game1 _game;
        private ContentResourceManager _content;


        private Texture2D _yellowStar;
        private Texture2D _empty_E_Overlay;
        private Texture2D _emptyPix;
        private Texture2D _mouseHoverOverlay;
        private Texture2D _buy_b_overlay;
        private Texture2D _sell_s_overlay;
        private Texture2D _selectedStar;

        private Tile[,] _map;
        private Node[,] _pathMap;

        public int TileResolution { get { return _game.cTileResolution; } }
        public int MapSize { get { return _game.cMapSize; } }

        public Tile[,] pMap { get { return _map; } }
        public Node[,] nMap { get { return _pathMap; } }

        public Map(Game1 game)
        {
            _game = game;
            _content = _game.ContentManager;
            IsLoaded = false;
        }

        public void Initialize()
        {
            _rnd = new Random();
            _map = new Tile[MapSize, MapSize];
            _pathMap = new Node[MapSize, MapSize];

            Instance = this;            
        }

        public void LoadContent()
        {
            _yellowStar = _content.Textures["yellowstar2"];
            _empty_E_Overlay = _content.Textures["emptyOverlay"];
            _emptyPix = _content.Textures["empty"];
            _mouseHoverOverlay = _content.Textures["hovertex"];
            
            _buy_b_overlay = _content.Textures["buyOverlay"];
            _sell_s_overlay = _content.Textures["sellOverlay"];
            _selectedStar = _content.Textures["selectedStar"];
        }

        public void StartGame()
        {
            int tileidcntr = 0;
            for (int y = 0; y < MapSize; y++)
            {
                for (int x = 0; x < MapSize; x++)
                {
                    Tile t = new Tile("Tile (" + x.ToString() + ", " + y.ToString() + ")", tileidcntr, _emptyPix, new Point(x, y), new Point(x * TileResolution, y * TileResolution));
                    Node n = new Node();
                    n.X = x;
                    n.Y = y;
                    t.Node = n;

                    _map[x, y] = t;
                    _pathMap[x, y] = n;

                    tileidcntr++;
                }
            }

            int starcount = _rnd.Next(10, 30);
            for (int s = 0; s < starcount; s++)
            {
                int econ = _rnd.Next(0, 8);
                Point rp = new Point(_rnd.Next(0, MapSize), _rnd.Next(0, MapSize));
                Star nextStar = new Star("Star: " + s.ToString(), _yellowStar, _selectedStar, rp, econ);
                nextStar.InitializeMarket();
                _map[rp.X, rp.Y].StarOnTile = nextStar;
            }

            IsLoaded = true;
        }

        public void Update(GameTime gameTime)
        {
            if (IsLoaded)
            {
                foreach (Tile t in _map)
                {
                    t.Update(gameTime);
                }
            }
        }


        public void Draw(GameTime gameTime)
        {
            if (IsLoaded)
            {
                for (int x = 0; x < MapSize; x++)
                {
                    for (int y = 0; y < MapSize; y++)
                    {
                        _game.SpriteBatch.Draw(_map[x, y].Sprite, _map[x, y].Bounds, Color.White);

                        if (_map[x, y].HitTest())
                        {
                            _game.SpriteBatch.Draw(_mouseHoverOverlay, new Rectangle(_map[x, y].ScreenPosition.X, _map[x, y].ScreenPosition.Y, TileResolution, TileResolution), Color.White);
                        }
                    }
                }
            }
        }

        public Tile GetTileAtPosition(Point pos)
        {
            for (int y = 0; y < MapSize; y++)
            {
                for (int x = 0; x < MapSize; x++)
                {
                    if(_map[x,y].Bounds.Contains(pos))
                    {
                        return _map[x, y];
                    }
                }
            }

            return null;
        }

        public Tile GetTileAtPosition(int mapx, int mapy)
        {

            return _map[mapx, mapy];
        }

        public int GetNodeMapCost(int x, int y)
        {
            if((x < 0) || (x > MapSize - 1))
            {
                return -1;
            }

            if((y < 0) || (y > MapSize - 1))
            {
                return -1;
            }

            if(_map[x,y].IsOpen)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        public void ClearGameMap()
        {
            for (int y = 0; y < MapSize; y++)
            {
                for (int x = 0; x < MapSize; x++)
                {
                    _map[x, y] = null;
                    _pathMap[x, y] = null;
                }
            }

            IsLoaded = false;
        }
    }
}
