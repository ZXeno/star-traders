﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace StarTraders
{
    public class Tile
    {
        private bool _isOpen;
        private string _name;
        private int _id;
        private Texture2D _sprite;
        private Point _screenPosition;
        private Point _mapPosition;
        private Star _starOnTile;

        public Node Node;

        public bool IsOpen 
        { 
            get { return _isOpen; }
            set { _isOpen = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int IDNumber
        {
            get { return _id; }
            protected set { _id = value; }
        }

        public Texture2D Sprite
        {
            get { return _sprite; }
        }

        public Point ScreenPosition
        {
            get { return _screenPosition; }
            set { _screenPosition = value; }
        }

        public Point MapPosition
        {
            get { return _mapPosition; }
            set { _mapPosition = value; }
        }

        public Rectangle Bounds
        {
            get { return new Rectangle(_screenPosition.X, _screenPosition.Y, 16, 16); }
        }

        public Star StarOnTile
        {
            get { return _starOnTile; }
            set { _starOnTile = value; }
        }


        public Tile(string name, int id, Texture2D sprite, Point mapPos, Point scrnPos)
        {
            _name = name;
            _id = id;
            _sprite = sprite;
            _mapPosition = mapPos;
            _screenPosition = scrnPos;
            _isOpen = true;
        }

        public void Update(GameTime gameTime)
        {

        }

        public bool HitTest()
        {
            if (Bounds.Contains(Camera2D.Main.ScreenToWorldPoint(Camera2D.Main._transform)))
            {
                return true;
            }

            return false;
        }

        
    }
}
