﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace StarTraders
{
    public class Player
    {
        public static Player Current { get; private set; }

        private Tile _selectedTile;
        private Entity _selectedEntity;
        public Tile SelectedTile
        {
            get { return _selectedTile; }
            set { _selectedTile = value; }
        }
        public Entity SelectedEntity
        {
            get { return _selectedEntity; }
            set 
            { 
                _selectedEntity = value;
                SFXManager.Instance.PlayClick();
            }
        }

        private List<Star> _ownedStars;
        public List<Star> OwnedStars
        {
            get { return _ownedStars; }
            set { _ownedStars = value; }
        }

        private List<Ship> _ownedShips;
        public List<Ship> OwnedShips 
        {
            get { return _ownedShips; }
            private set { _ownedShips = value; }
        }

        private float _money;
        public float Money
        {
            get { return _money; }
            set { _money = value; }
        }

        private int _shipIDCounter = 0;
        public int ShipIDCounter 
        { 
            get { return _shipIDCounter; }
            set { _shipIDCounter = value; }
        }
        public List<Ship> ShipsNeedingDestination { get; set; }

        public Player()
        {
            _ownedStars = new List<Star>();
            _ownedShips = new List<Ship>();
            ShipsNeedingDestination = new List<Ship>();
            Current = this;
            _money = 1000;
        }

        public int GetAllShipsCargoCount()
        {
            int count = 0;

            if (OwnedShips != null && OwnedShips.Count > 0)
            {
                for (int s = 0; s < OwnedShips.Count; s++)
                {
                    for (int i = 0; i < OwnedShips[s].CargoHold.Goods.Count; i++)
                    {
                        if (OwnedShips[s].CargoHold.Goods[i].Quantity > 0)
                        {
                            count += OwnedShips[s].CargoHold.Goods[i].Quantity;
                        }
                    }
                }
            }

            return count;
        }
    }
}
