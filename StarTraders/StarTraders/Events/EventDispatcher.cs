﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StarTraders
{
    public static class Events
    {
        public static event EventHandler<Star> OpenStarWindow;
        public delegate void EventHandler<Star>(Star args);
        public static void OnOpenStarWindow(Star args)
        {
            if (OpenStarWindow != null)
            {
                OpenStarWindow(args);
            }
        }

        public static event DockEvent DockOccurred;
        public delegate void DockEvent(Star arg1, Ship arg2);
        public static void OnShipDock(Star arg1, Ship arg2)
        {
            if (DockOccurred != null)
            {
                DockOccurred(arg1, arg2);
            }
        }

        public static event DockEvent UnDockOccurred;
        public static void OnShipUnDock(Star arg1, Ship arg2)
        {
            if (UnDockOccurred != null)
            {
                UnDockOccurred(arg1, arg2);
            }
        }
    }
}
