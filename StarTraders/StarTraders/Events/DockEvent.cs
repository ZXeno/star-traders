﻿using System;
using Microsoft.Xna.Framework;

namespace StarTraders
{
    public class DockEvent : EventArgs
    {
        public Ship Ship { get; set; }
        public Star Star { get; set; }
        
        /// <summary>
        /// Specifies which ship and which Star are involved in the dock event;
        /// </summary>
        /// <param name="star"></param>
        /// <param name="ship"></param>
        public DockEvent(Star star, Ship ship)
        {
            Ship = ship;
            Star = star;
        }

        
    }
}
