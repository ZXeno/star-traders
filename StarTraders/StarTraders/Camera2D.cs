﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Utilities;

namespace StarTraders
{
    public class Camera2D
    {
        private static Camera2D _instance;
        public static Camera2D Main { get { return _instance; } }

        protected float _zoom; // Camera Zoom
        public Matrix _transform; // Matrix Transform
        public Vector2 _pos; // Camera Position
        protected float _rotation; // Camera Rotation

        public float Zoom
        {
            get { return _zoom; }
            set 
            { 
                _zoom = value;
                if (_zoom < 0.1f)
                {
                    _zoom = 0.1f;
                }
                else if (_zoom > 1.0f)
                {
                    _zoom = 1.0f;
                }
            } // Negative zoom will flip image
        }

        public float Rotation
        {
            get { return _rotation; }
            set { _rotation = value; }
        }

        // Get set position
        public Vector2 Pos
        {
            get { return _pos; }
            set { _pos = value; }
        }

        public Camera2D()
        {
            _zoom = 1.0f;
            _rotation = 0.0f;
            _pos = Vector2.Zero;
            _instance = this;
        }

        public Matrix GetTransformation(GraphicsDevice graphicsDevice)
        {
            _transform =       // Thanks to o KB o for this solution
              Matrix.CreateTranslation(new Vector3(-_pos.X, -_pos.Y, 0)) *
                                         Matrix.CreateRotationZ(Rotation) *
                                         Matrix.CreateScale(new Vector3(Zoom, Zoom, 1)) *
                                         Matrix.CreateTranslation(new Vector3(graphicsDevice.Viewport.Width * 0.5f, graphicsDevice.Viewport.Height * 0.5f, 0));
            return _transform;
        }

        public void HandleInput(GameTime gameTime)
        {
            float yposchange = Pos.Y;
            float xposchange = Pos.X;

            if (Input.GetKey(Microsoft.Xna.Framework.Input.Keys.W))
            {
                yposchange = Pos.Y - 1.5f * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            }
            if (Input.GetKey(Microsoft.Xna.Framework.Input.Keys.S))
            {
                yposchange = Pos.Y + 1.5f * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            }
            if (Input.GetKey(Microsoft.Xna.Framework.Input.Keys.D))
            {
                xposchange = Pos.X + 1.5f * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            }
            if (Input.GetKey(Microsoft.Xna.Framework.Input.Keys.A))
            {
                xposchange = Pos.X - 1.5f * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            }

            Pos = new Vector2(xposchange, yposchange);
        }

        /// <summary>
        /// Return the 2D world point of the mouse.
        /// </summary>
        /// <param name="ViewMatrix"></param>
        /// <returns>Vector2</returns>
        public Vector2 ScreenToWorldPoint(Matrix ViewMatrix)
        {
            Vector2 worldPoint = new Vector2();

            Matrix inverseViewMatrix = Matrix.Invert(ViewMatrix);
            worldPoint = Vector2.Transform(Input.MousePosition.ToVector2(), inverseViewMatrix);

            return worldPoint;
        }

        public Vector2 ScreenToWorldPoint(Vector2 Position, Matrix ViewMatrix)
        {
            Vector2 worldPoint = new Vector2();

            Matrix inverseViewMatrix = Matrix.Invert(ViewMatrix);
            worldPoint = Vector2.Transform(Position, inverseViewMatrix);

            return worldPoint;
        }
    }
}
