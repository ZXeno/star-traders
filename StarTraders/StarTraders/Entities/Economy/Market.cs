﻿using System;
using System.Collections.Generic;

namespace StarTraders
{

    public class Market
    {
        Random _rnd { get { return GameManager.RND; } }

        // This market's definitions
        private List<Good> _goods;

        private int _economy;

        // This Market
        public List<Good> Goods { get { return _goods; } }
        

        public Market(int economy)
        {
            _goods = new List<Good>();
            _economy = economy;
        }

        public void InitializeMarket()
        {
            _goods.Add(Good.NewFood());
            _goods.Add(Good.NewFuel());
            _goods.Add(Good.NewIndustrialGoods());
            _goods.Add(Good.NewLuxuries());
        }

        public void SetMarketPrices()
        {
            SetThisMarketPrices();
            SetThisMarketQuantities();
        }

        public float GetMarketValue(string goodname)
        {

            return (float)Math.Round(_goods.Find(g => g.Name == goodname).Price, 2);
        }

        public void AddQuantity(string goodname, int amount)
        {
            _goods.Find(g => g.Name == goodname).Quantity += amount;
        }

        public void SubtractQuantity(string goodname, int amount)
        {
            Good gtc = _goods.Find(g => g.Name == goodname);

            if (gtc != null)
            {
                if (gtc.Quantity > amount)
                {
                    gtc.Quantity -= amount;
                }
            }
        }

        public int GetTotalQuantity()
        {
            int count = 0;
            foreach(Good g in _goods)
            {
                count += g.Quantity;
            }

            return count;
        }

        public static string GetMarketLabel(int encomyType)
        {
            switch (encomyType)
            {
                case 0:
                    return "Rich Industrial";
                case 1:
                    return "Average Industrial";
                case 2:
                    return "Poor Industrial";
                case 3:
                    return "Somewhat Industrial";
                case 4:
                    return "Somewhat Agricultural";
                case 5:
                    return "Rich Agricultural";
                case 6:
                    return "Average Agricultural";
                case 7:
                    return "Poor Agricultural";
                default:
                    return "YOU_SHOULD_NEVER_SEE_THIS_VALUE";
            }
        }

        private void SetThisMarketPrices()
        {
            foreach (Good good in _goods)
            {
                good.Price = (good.MarketBasePrice + (_rnd.Next(256) & good.MarketMaskPrice) + (_economy * good.MarketEcoAdjustPrice)) & 255;

                good.Price *= 0.8f;
            }
        }

        private void SetThisMarketQuantities()
        {
            foreach (Good good in _goods)
            {
                good.Quantity = (good.MarketBaseQuantity + (_rnd.Next(256) & good.MarketMaskQuantity) + (_economy * good.MarketEcoAdjustQuantity)) & 255;

                if (good.Quantity < 0)
                {
                    good.Quantity = 0;
                }
            }
        }

    }
}
