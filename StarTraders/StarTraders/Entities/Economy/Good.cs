﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StarTraders
{
    public class Good
    {
        public string Name { get; set; }
        public int Quantity { get; set; }
        public float Price { get; set; }
        public int MarketBasePrice { get; set; }
        public int MarketEcoAdjustPrice { get; set; }
        public int MarketEcoAdjustQuantity { get; set; }
        public int MarketBaseQuantity { get; set; }
        public int MarketMaskPrice { get; set; }
        public int MarketMaskQuantity { get; set; }

        

        public Good()
        {
            Name = "";
            Quantity = 0;
            Price = 0;
            MarketBasePrice = 0;
            MarketEcoAdjustPrice = 0;
            MarketEcoAdjustQuantity = 0;
            MarketBaseQuantity = 0;
            MarketMaskPrice = 0;
            MarketMaskQuantity = 0;
        }

        public static Good NewFood()
        {
            Good newgood = new Good();

            newgood.Name = "Food";
            newgood.Quantity = 0;
            newgood.Price = 0;
            newgood.MarketBasePrice = 50;
            newgood.MarketEcoAdjustPrice = -2;
            newgood.MarketEcoAdjustQuantity = -2;
            newgood.MarketBaseQuantity = 6;
            newgood.MarketMaskPrice = 1;
            newgood.MarketMaskQuantity = 1;

            return newgood;
        }

        public static Good NewLuxuries()
        {
            Good newgood = new Good();

            newgood.Name = "Luxuries";
            newgood.Quantity = 0;
            newgood.Price = 0;
            newgood.MarketBasePrice = 208;
            newgood.MarketEcoAdjustPrice = 5;
            newgood.MarketEcoAdjustQuantity = 8;
            newgood.MarketBaseQuantity = 54;
            newgood.MarketMaskPrice = 3;
            newgood.MarketMaskQuantity = 3;

            return newgood;
        }

        public static Good NewIndustrialGoods()
        {
            Good newgood = new Good();

            newgood.Name = "Industrial Goods";
            newgood.Quantity = 0;
            newgood.Price = 0;
            newgood.MarketBasePrice = 117;
            newgood.MarketEcoAdjustPrice = 6;
            newgood.MarketEcoAdjustQuantity = 6;
            newgood.MarketBaseQuantity = 40;
            newgood.MarketMaskPrice = 7;
            newgood.MarketMaskQuantity = 7;

            return newgood;
        }

        public static Good NewFuel()
        {
            Good newgood = new Good();

            newgood.Name = "Fuel";
            newgood.Quantity = 0;
            newgood.Price = 0;
            newgood.MarketBasePrice = 95;
            newgood.MarketEcoAdjustPrice = -3;
            newgood.MarketEcoAdjustQuantity = -3;
            newgood.MarketBaseQuantity = 2;
            newgood.MarketMaskPrice = 4;
            newgood.MarketMaskQuantity = 7;

            return newgood;
        }

    }
}
