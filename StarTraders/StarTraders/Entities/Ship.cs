﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace StarTraders
{
    public class Ship : Entity
    {
        private Tile _currentLocation;
        private Tile _destination;
        private bool _doNaviation = false;
        private bool _isDocked = false;
        private bool _isAlive = true;
        private Stack<Point> _path;
        private Queue<Tile> _pathQueue;
        private Astar _pather;

        private Texture2D _selectedSprite;
        private Texture2D _normalSprite;

        private Market _cargoHold;

        private int _cargoCap = 4;
        private int _fuel = 100;
        private int _fuelRate = 1;
        private int _maxFuel = 100;
        private int _cargoUpgradeMax = 10;
        private int _fuelUpgradeMax = 200;
        private float _cargoUpgradeCost = 250;
        private float _fuelUpgradeCost = 150;


        public int CargoCapacity { get { return _cargoCap; } }
        public int AvailableCargoSpace { get { return (_cargoCap - _cargoHold.GetTotalQuantity()); } }
        public int CurrentCargoCount { get { return _cargoHold.GetTotalQuantity(); } }
        public int Fuel { get { return _fuel; } }
        public int FuelRate { get { return _fuelRate; } }
        public int MaxFuel { get { return _maxFuel; } }
        public int CargoUpgradeMax { get { return _cargoUpgradeMax; } }
        public int FuelUpgradeMax { get { return _fuelUpgradeMax; } }
        public float CargoUpgradeCost { get { return _cargoUpgradeCost; } }
        public float FuelUpgradeCost { get { return _fuelUpgradeCost; } }
        public Tile CurrentLocation { get { return _currentLocation; } }
        public Tile Destination { get { return _destination; } }
        public Queue<Tile> PathQueue { get { return _pathQueue; } }
        public Stack<Point> Path { get { return _path; } }
        public Market CargoHold { get { return _cargoHold; } }
        public bool IsDocked
        {
            get { return _isDocked; }
            set { _isDocked = value; }
        }

        public Ship(string name, Texture2D sprite, Texture2D selected, Point mappos) 
            : base(name, sprite, mappos, false)
        {
            _currentLocation = Map.Instance.GetTileAtPosition(mappos.X, mappos.Y);
            _selectedSprite = selected;
            _normalSprite = sprite;
            _pathQueue = new Queue<Tile>();
            _path = new Stack<Point>();
            _cargoHold = new Market(0);
            _cargoHold.InitializeMarket();
            Player.Current.OwnedShips.Add(this);
            _pather = new Astar(Map.Instance.nMap);
            Player.Current.ShipsNeedingDestination.Add(this);

            Events.UnDockOccurred += Undock;
        }

        public override void HandleInput()
        {
            if (!UIManager.MouseIsOverUI)
            {
                if (Player.Current.SelectedEntity == this)
                {
                    // if we right click, set destination or queue a destination
                    if (Input.GetButtonDown(1))
                    {
                        if (Input.GetKey(Microsoft.Xna.Framework.Input.Keys.LeftShift))
                        {
                            Tile nextdest = Map.Instance.GetTileAtPosition(Camera2D.Main.ScreenToWorldPoint(Camera2D.Main._transform).ToPoint());
                            if (nextdest != null)
                            {
                                _pathQueue.Enqueue(nextdest);
                            }
                        }
                        else
                        {
                            Tile nextdest = Map.Instance.GetTileAtPosition(Camera2D.Main.ScreenToWorldPoint(Camera2D.Main._transform).ToPoint());
                            if (nextdest != null)
                            {
                                StopNavigation();
                                SetDestination(nextdest);
                            }
                        }

                        SFXManager.Instance.PlaySetDest();
                    }
                }

                // if we aren't already selected and we left click...
                else
                {
                    if (Input.GetButtonDown(0))
                    {
                        if (Player.Current.SelectedEntity != this && this.HitTest())
                        {
                            // select us
                            Player.Current.SelectedEntity = this;
                        }
                    }
                }
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (_isAlive)
            {
                // set the correct sprite if we're selected or not
                if (Player.Current.SelectedEntity == this)
                {
                    if (Sprite != _selectedSprite)
                    {
                        Sprite = _selectedSprite;
                    }
                }
                else
                {
                    if (Sprite != _normalSprite)
                    {
                        Sprite = _normalSprite;
                    }
                }
            }
        }

        public override void TickUpdate()
        {
            if (_isAlive)
            {
                if (_pathQueue.Count > 0 && !_doNaviation)
                {
                    SetDestination(_pathQueue.Dequeue());
                    _doNaviation = true;
                }

                if (((_pathQueue.Count == 0 && _path.Count == 0) || !_doNaviation) && !_isDocked)
                {
                    if (!Player.Current.ShipsNeedingDestination.Contains(this))
                    {
                        Player.Current.ShipsNeedingDestination.Add(this);
                    }
                }

                if (_doNaviation)
                {
                    if (Player.Current.ShipsNeedingDestination.Contains(this)) 
                    { 
                        Player.Current.ShipsNeedingDestination.Remove(this); 
                    }

                    Navigate();
                }

                if(Fuel == 0)
                {
                    DestroyShip();
                }
            }
        }


        public void SetDestination(Tile t)
        {
            if (!_isDocked)
            {
                _destination = t;
                _doNaviation = true;
            }
            else
            {
                _pathQueue.Enqueue(t);
            }
        }

        // navigate!
        public void Navigate()
        {
            if (_isAlive)
            {
                if (!_isDocked)
                {
                    if (_path == null || _path.Count == 0)
                    {
                        _path = _pather.AstarPath(_currentLocation.Node, _destination.Node);
                    }

                    if (_path != null && _path.Count > 0)
                    {
                        Point n = _path.Pop();
                        _currentLocation = Map.Instance.GetTileAtPosition(n.X, n.Y);
                        MapPosition = _currentLocation.MapPosition;
                        _fuel -= _fuelRate;

                        if (MapPosition == _destination.MapPosition)
                        {
                            _doNaviation = false;
                            
                            if (_destination.StarOnTile != null)
                            {
                                _isDocked = true;
                                _destination.StarOnTile.DockShip(this);
                            }

                            _currentLocation = _destination;
                            _path.Clear();
                            _destination = null;

                            if(_pathQueue.Count > 0)
                            {
                                SetDestination(_pathQueue.Dequeue());
                            }
                        }
                    }
                }
            }
        }

        private void StopNavigation()
        {
            _doNaviation = false;
            _path.Clear();
            _pathQueue.Clear();
        }

        public void Undock(Star star, Ship ship)
        {
            if (star == _currentLocation.StarOnTile && ship == this)
            {
                _isDocked = false;
                SFXManager.Instance.PlayUndock();
                if (_pathQueue != null && _pathQueue.Count > 0)
                {
                    Tile newdest = _pathQueue.Dequeue();
                    _doNaviation = true;
                    if (newdest != null && newdest != _currentLocation)
                    {
                        _destination = newdest;
                    }
                    else
                    {
                        Player.Current.ShipsNeedingDestination.Add(this);
                        _doNaviation = false;
                    }
                }

            }
        }

        public void Refuel(Star sourceStar)
        {
            int diff = MaxFuel - Fuel;
            float cost = (float)Math.Round((((sourceStar.MarketGoods.Find(g => g.Name == "Fuel").Price) * diff) * .1f), 2);

            if (Player.Current.Money > cost)
            {
                _fuel += diff;
                Player.Current.Money -= cost;
            }

        }

        public void BuyCargoUpgrade()
        {
            if(CargoCapacity < 10)
            {
                _cargoCap++;
                _cargoUpgradeCost += (float)Math.Round((_cargoUpgradeCost * .25f), 2);
            }
        }

        public void BuyFuelTankUpgrade()
        {
            if(MaxFuel < 200)
            {
                _maxFuel += 10;
                _fuelUpgradeCost += (float)Math.Round((_fuelUpgradeCost * .15f), 2);
            }
        }

        public void DestroyShip()
        {
            GameManager.Instance.RemoveEntity(this);
            if (Player.Current.SelectedEntity == this)
            {
                Player.Current.SelectedEntity = null;
            }
            Player.Current.OwnedShips.Remove(this);
            IsEnabled = false;
            _isAlive = false;
            SFXManager.Instance.PlayRandomExplosion();
            _path = null;
            _pathQueue = null;
            _name = "";
            _maxFuel = 0;
            _fuel = 0;
            _cargoCap = 0;
            _cargoHold = null;
            _currentLocation = null;
            _destination = null;
            MapPosition = Point.Zero;
        }
    }
}
