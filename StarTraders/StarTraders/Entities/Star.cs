﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace StarTraders
{
    public class Star : Entity
    {
        private Random rnd = new Random();

        private Texture2D _selected;
        private Texture2D _default;

        private int _economy; // must be value between 0 - 7 (non-inclusive 8)
        private Market _market;

        public List<Ship> ShipsInSystem;
        public Texture2D CurrentOverlay { get; protected set; }
        public int EconomyType { get { return _economy; } }
        public List<Good> MarketGoods { get { return _market.Goods; } }

        public Star(string name, Texture2D sprite, Texture2D selected, Point mappos, int economytype)
            : base(name, sprite, mappos, true)
        {
            ShipsInSystem = new List<Ship>();
            _default = sprite;
            _selected = selected;
            _name = name;
            _economy = economytype;
            _market = new Market(_economy);
        }

        public void InitializeMarket()
        {
            _market.InitializeMarket();
            _market.SetMarketPrices();
        }

        public override void Update(GameTime gameTime) 
        {
            // set the correct sprite if we're selected or not
            if (Player.Current.SelectedEntity == this)
            {
                if (Sprite != _selected)
                {
                    Sprite = _selected;
                }
            }
            else
            {
                if (Sprite != _default)
                {
                    Sprite = _default;
                }
            }

            if(ShipsInSystem.Count > 0)
            {
                for(int i = 0; i < ShipsInSystem.Count; i++)
                {
                    if(ShipsInSystem[i].CurrentLocation.MapPosition != this.MapPosition)
                    {
                        ShipsInSystem.Remove(ShipsInSystem[i]);
                    }
                }
            }
        }


        // Tick update!
        public override void TickUpdate()
        {

        }

        public void DockShip(Ship s)
        {
            ShipsInSystem.Add(s);
            Events.OnShipDock(this, s);
            SFXManager.Instance.PlayDock();
        }

        public override void HandleInput()
        {
            if (!UIManager.MouseIsOverUI)
            {
                if(Input.GetButtonDown(0) && HitTest())
                {
                    if (ShipsInSystem.Count == 0)
                    {
                        Player.Current.SelectedEntity = this;
                    }
                    else
                    {
                        Player.Current.SelectedEntity = ShipsInSystem[0];
                    }
                }
            }
        }
    }
}
