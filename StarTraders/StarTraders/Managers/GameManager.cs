﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace StarTraders
{

    public class GameManager
    {
        public static GameManager Instance { get; private set; }
        private static Random _rand = new Random();
        public static Random RND { get { return _rand; } }
        

        // General game vars
        private Game1 _game;
        private ContentResourceManager _content;
        private Random _rnd;
        private bool _gameStarted;
        public bool GameStarted { get { return _gameStarted; } }
        private GameplayState _currPlayState;
        public GameplayState CurrentPlayState 
        { 
            get { return _currPlayState; } 
            set { _currPlayState = value; } 
        }
        public string EndGameConditionText { get; set; }

        // Map vars
        public Map _map;
        public int TileResolution { get { return _game.cTileResolution; } }
        public int MapSize { get { return _game.cMapSize; } }

        // Player
        private Player _player;
        public Player Player { get { return _player; } }
        public Camera2D Camera { get; set; }

        // Game Timer
        private double _prevTickTime = 0.0;
        private double _nextTickTime = .5;
        private double _tickTimer = 0;

        // Entities
        private Texture2D _ship1;
        private Texture2D _selectedShipSprite;

        public List<Entity> AllEntities { get; set; }
        public List<Entity> EntitiesToRemove { get; private set; }

        // Constructor
        public GameManager(Game1 game) 
        {
            _game = game;
            _content = game.ContentManager;
            Instance = this;
        }


        public void Initialize()
        {
            _rnd = new Random();
            _map = new Map(_game);
            _player = new Player();
            EndGameConditionText = "";
            _gameStarted = false;
            Camera = new Camera2D();

            AllEntities = new List<Entity>();
            EntitiesToRemove = new List<Entity>();

            _map.Initialize();
            CurrentPlayState = GameplayState.Playing;
        }

        public void LoadContent()
        {
            _ship1 = _content.Textures["ship1"];
            _selectedShipSprite = _content.Textures["selectedship"];

            // load map content
            _map.LoadContent();
        }

        public void Update(GameTime gameTime)
        {
            
            if (CurrentPlayState == GameplayState.Playing)
            {
                HandleInput(gameTime);

                // if we have entities that were destroyed last frame
                // remove them from the update/draw loop this frame
                if (EntitiesToRemove.Count > 0)
                {
                    foreach (Entity e in EntitiesToRemove)
                    {
                        AllEntities.Remove(e);
                    }

                    EntitiesToRemove.Clear();
                }

                _tickTimer += gameTime.ElapsedGameTime.TotalSeconds;

                // Update/Handle input for all entities, regardless of tick
                foreach (Entity e in AllEntities)
                {
                    if (e.IsEnabled)
                    {
                        e.Update(gameTime);
                        e.HandleInput();
                    }
                }

                


                // Run the tickupdate for all entities
                if (_prevTickTime + _nextTickTime <= _tickTimer)
                {
                    foreach (Entity e in AllEntities)
                    {
                        if (e.IsEnabled)
                            e.TickUpdate();
                    }

                    _prevTickTime = _tickTimer;
                }

                // Update map
                _map.Update(gameTime);

                CheckWin();
                CheckLoss();
                
            }
        }

        private void HandleInput(GameTime gameTime)
        {
            Camera.HandleInput(gameTime);

            Vector2 newpos = Camera.Pos;
            if(Camera.Pos.X < _map.GetTileAtPosition(0,0).ScreenPosition.X)
            {
                newpos = new Vector2(_map.GetTileAtPosition(0, 0).ScreenPosition.X, newpos.Y);
            }

            if(Camera.Pos.X > _map.GetTileAtPosition(_map.MapSize - 1,0).ScreenPosition.X)
            {
                newpos = new Vector2(_map.GetTileAtPosition(_map.MapSize - 1, 0).ScreenPosition.X, newpos.Y);
            }

            if (Camera.Pos.Y < _map.GetTileAtPosition(0, 0).ScreenPosition.Y)
            {
                newpos = new Vector2(newpos.X, _map.GetTileAtPosition(0, 0).ScreenPosition.Y);
            }

            if (Camera.Pos.Y > _map.GetTileAtPosition(0, _map.MapSize - 1).ScreenPosition.Y)
            {
                newpos = new Vector2(newpos.X, _map.GetTileAtPosition(0, _map.MapSize - 1).ScreenPosition.Y);
            }

            Camera.Pos = newpos;

            if ((Input.GetButton(0) || Input.GetButton(1)) && !UIManager.MouseIsOverUI)
            {
                Player.Current.SelectedTile = _map.GetTileAtPosition(Camera2D.Main.ScreenToWorldPoint(Camera2D.Main._transform).ToPoint());
            }
        }

        public void Draw(GameTime gameTime)
        {
            _game.SpriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, Camera.GetTransformation(_game.GraphicsDevice));

            _map.Draw(gameTime);

            foreach (Entity e in AllEntities)
            {
                _game.SpriteBatch.Draw(e.Sprite, e.Bounds, Color.White);

                if (e is Star)
                {
                    Star s = e as Star;
                    if (s.CurrentOverlay != null)
                    {
                        _game.SpriteBatch.Draw(s.CurrentOverlay, e.Bounds, Color.White);
                    }
                }
            }

            _game.SpriteBatch.End();
        }

        public void StartGame()
        {
            _map.StartGame();
            CreateShip();
            _gameStarted = true;
            Camera.Pos = _map.pMap[30, 30].ScreenPosition.ToVector2();
        }

        public void CreateShip()
        {
            Ship newShip = new Ship("Ship " + Player.Current.ShipIDCounter.ToString(), _ship1, _selectedShipSprite, new Point(30, 30));
            Player.Current.ShipIDCounter++;
        }

        public void BuyShip()
        {
            if (Player.Current.OwnedShips.Count > 0 && Player.Current.Money > 5000)
            {
                Player.Current.Money -= 5000;
                CreateShip();
            }
        }

        public void RemoveEntity(Entity etr)
        {
            EntitiesToRemove.Add(etr);
        }

        public void ResetAll()
        {
            ResetPlayer();
            ResetTimer();
            RemoveAllEntities();
            _map.ClearGameMap();
        }

        private void CheckWin()
        {
            if (Player.Current.Money > 100000)
            {
                CurrentPlayState = GameplayState.Win;
                EndGameConditionText = "CONGRATULATIONS! \n You have earned over $100,000 and can now retire to a tropical ocean world!";
            }
        }

        private void CheckLoss()
        {
            if(Player.Money <= 0)
            {
                CurrentPlayState = GameplayState.Loss;
                EndGameConditionText = "YOU HAVE RUN OUT OF MONEY!";
            }

            if(Player.Current.Money <= 26 && Player.Current.GetAllShipsCargoCount() == 0)
            {
                CurrentPlayState = GameplayState.Loss;
                EndGameConditionText = "YOU HAVE RUN OUT OF MONEY!";
            }

            if(Player.Current.OwnedShips.Count == 0)
            {
                CurrentPlayState = GameplayState.Loss;
                EndGameConditionText = "YOU HAVE NO MORE SHIPS!";
            }
        }

        private void RemoveAllEntities()
        {
            AllEntities.Clear();
            EntitiesToRemove.Clear();
        }

        private void ResetPlayer()
        {
            Player.Current.OwnedShips.Clear();
            Player.Current.OwnedStars.Clear();
            Player.Current.ShipsNeedingDestination.Clear();
            Player.Current.Money = 10000;
            Player.Current.ShipIDCounter = 0;
            Player.Current.SelectedEntity = null;
            Player.Current.SelectedTile = null;
        }

        private void ResetTimer()
        {
            _tickTimer = 0;
            _prevTickTime = 0;
        }

    }
}
