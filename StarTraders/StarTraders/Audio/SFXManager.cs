﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Utilities;

namespace StarTraders
{
    public class SFXManager : IDisposable
    {
        public static SFXManager Instance { get; private set; }

        private bool _playlistCheck = false;

        private Game1 _game;
        private ContentResourceManager _content;
        private Random _rnd;

        private Song _gameSong1;
        private Song _gameSong2;
        private Song _failureSong;
        private Song _menuSong;

        private int _currentSong = 0;

        private SoundEffect _clickBlip;
        private SoundEffect _setdestblip;

        private SoundEffect _undock;
        private SoundEffect _dock;

        private List<Song> _playlist;
        private List<SoundEffect> _xplosions;

        public Song GameSong1 { get { return _gameSong1; } }
        public Song GameSong2 { get { return _gameSong2; } }
        public Song MenuSong { get { return _menuSong; } }
        public Song FailureSong { get { return _failureSong; } }

        public SoundEffect ClickBlip1 { get { return _clickBlip; } }

        public SoundEffect BuyUndock1 { get { return _undock; } }

        public SFXManager(Game1 game)
        {
            _game = game;
            _content = game.ContentManager;
            Instance = this;
            _rnd = new Random();
            _playlist = new List<Song>();
            _xplosions = new List<SoundEffect>();

            MediaPlayer.Volume = .1f;
            _game.StateManager.OnStateChange += StateJustChanged;
        }

        public void LoadContent()
        {
            _clickBlip = _content.SoundFX["click_blip"];
            _undock = _content.SoundFX["undock"];
            _dock = _content.SoundFX["dock"];
            _setdestblip = _content.SoundFX["set_dest1"];

            _xplosions.Add(_content.SoundFX["xplode0"]);
            _xplosions.Add(_content.SoundFX["xplode1"]);
            _xplosions.Add(_content.SoundFX["xplode2"]);
            _xplosions.Add(_content.SoundFX["xplode3"]);
            _xplosions.Add(_content.SoundFX["xplode4"]);
            _xplosions.Add(_content.SoundFX["xplode5"]);
            _xplosions.Add(_content.SoundFX["xplode6"]);
            _xplosions.Add(_content.SoundFX["xplode7"]);
            _xplosions.Add(_content.SoundFX["xplode8"]);
            _xplosions.Add(_content.SoundFX["xplode9"]);

            _gameSong1 = _content.Songs["Artuir - Mercantile Solitude"];
            _gameSong2 = _content.Songs["Artuir - Infinite Frontier"];
            _menuSong = _content.Songs["MENU"];
            _failureSong = _content.Songs["LOSE"];


            _playlist.Add(_gameSong1);
            _playlist.Add(_gameSong2);
        }

        public void Update()
        {
            HandleInput();

            if (MediaPlayer.State != MediaState.Playing && _game.CurrentState is GameplayScreen) 
            {
                if(_playlistCheck) 
                {
                    _playlistCheck = false;  
                    _currentSong++;
                    if (_currentSong > _playlist.Count - 1)
                    {
                        _currentSong = 0;
                    }
                }
                MediaPlayer.Play(_playlist[_currentSong]);  
            }

            if (MediaPlayer.State == MediaState.Playing && _game.CurrentState is GameplayScreen) 
            {
                _playlistCheck = true;  
            }
            
            if (MediaPlayer.State == MediaState.Playing && _game.CurrentState is GameplayScreen && GameManager.Instance.CurrentPlayState == GameplayState.Loss)
            {
                if(MediaPlayer.Queue.ActiveSong != _failureSong)
                {
                    MediaPlayer.Stop();
                    MediaPlayer.Play(_failureSong);
                }
            }

            if (_game.CurrentState is MainMenu)
            {
                if (MediaPlayer.Queue.ActiveSong != _menuSong)
                {
                    MediaPlayer.Stop();
                    MediaPlayer.Play(_menuSong);
                }
            }
        }

        private void HandleInput()
        {
            if (Input.GetKeyDown(Microsoft.Xna.Framework.Input.Keys.Down))
            {
                MediaPlayer.Volume -= 0.1f;
            }
            if (Input.GetKeyDown(Microsoft.Xna.Framework.Input.Keys.Up))
            {
                MediaPlayer.Volume += 0.1f;
            }

            if (Input.GetKeyDown(Microsoft.Xna.Framework.Input.Keys.Right) && _game.CurrentState is GameplayScreen)
            {
                MediaPlayer.Stop();
            }

            if (Input.GetKeyDown(Microsoft.Xna.Framework.Input.Keys.NumPad0))
            {
                ToggleMusic();
            }
        }

        public void PlayMusic(Song s)
        {
            if (MediaPlayer.State != MediaState.Playing)
            {
                MediaPlayer.Play(s);
            }
        }

        public void StopMusic()
        {
            MediaPlayer.Stop();
        }

        public void PlayEffect(SoundEffect sfx)
        {
            sfx.Play();
        }

        public void PlaySetDest()
        {
            PlayEffect(_setdestblip);
        }

        public void PlayDock()
        {
            PlayEffect(_dock);
        }

        public void PlayUndock()
        {
            PlayEffect(_undock);
        }

        public void PlayClick()
        {
            PlayEffect(_clickBlip);
        }

        public void PlayRandomExplosion()
        {
            PlayEffect(_xplosions[_rnd.Next(_xplosions.Count)]);
        }

        public void Dispose()
        {
            _game = null;
            _rnd = null;
            _xplosions.Clear();
            _gameSong1.Dispose();
            _gameSong2.Dispose();
            _failureSong.Dispose();
            _clickBlip.Dispose();
            _undock.Dispose();
        }

        private void StateJustChanged(object sender, object arg)
        {
            MediaPlayer.Stop();
            if(_game.CurrentState is MainMenu)
            {
                MediaPlayer.Play(_menuSong);
            }
        }

        private void ToggleMusic()
        {
            if(MediaPlayer.Volume == 1)
            {
                MediaPlayer.Volume = 0;
            }
            else
            {
                MediaPlayer.Volume = 1;
            }
        }
    }
}
