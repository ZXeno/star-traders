﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace StarTraders
{
    internal class Sprite
    {
        private Texture2D _sprite;
        private Point _position;
        private Vector2 _pivot;
        private readonly SpriteBatch _spriteBatch;
        private Color _color;

        public Texture2D Image { get { return _sprite; } }

        public Point Position 
        { 
            get { return _position; }
            set { _position = value; }
        }

        public Vector2 Pivot
        {
            get { return _pivot; }
            set { _pivot = value; }
        }

        public Vector2 PivotPoint
        {
            get { return new Vector2(_position.X + (_sprite.Width * _pivot.X), _position.Y + (_sprite.Height * _pivot.Y)); }
        }

        public Rectangle Bounds
        {
            get
            {
                return new Rectangle(
                    _position.X,
                    _position.Y,
                    _sprite.Width,
                    _sprite.Height
                    );
            }
        }

        public Sprite(Texture2D sprite, SpriteBatch spriteBatch)
        {
            _sprite = sprite;
            _spriteBatch = spriteBatch;
            _color = Color.White;
        }

        public Sprite(Texture2D sprite, SpriteBatch spriteBatch, Point position)
        {
            _sprite = sprite;
            _spriteBatch = spriteBatch;
            _position = position;
            _color = Color.White;
        }

        public void Update()
        {

        }

        public void Draw()
        {
            _spriteBatch.Draw(_sprite, Bounds, _color);
        }

        public void DrawSliced(Texture2D sprite, int cornerslice)
        {
            
        }
    }
}
