﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using System;
using System.Runtime.InteropServices;

namespace StarTraders
{

    public class Game1 : Game
    {
        public static Game1 Instance;

        private Input _input;
        private SFXManager _audio;
        private ContentResourceManager _contentManager;
        private const int _cTileResolution = 16;
        private const int _cGameViewResolution = 960;
        private const int _cMapSize = 60;
        private const int _cUIWdith = 256;

        private GameStateManager _stateManager;
        private MainMenu _mainMenu;
        private GameplayScreen _gameplayScreen;

        public GraphicsDeviceManager Graphics { get; private set; }
        public SpriteBatch SpriteBatch { get; private set; }

        public int cTileResolution { get { return _cTileResolution; } }
        public int cGameViewResolution { get { return _cGameViewResolution; } }
        public int cMapSize { get { return _cMapSize; } }
        public int cUIResolutionW { get { return _cUIWdith; } }
        public int cUIResolutionH { get { return Graphics.PreferredBackBufferHeight; } }

        protected Texture2D background;

        public ContentResourceManager ContentManager { get { return _contentManager; } }
        public MainMenu MainMenu { get { return _mainMenu; } }
        public GameplayScreen GameplayScreen { get { return _gameplayScreen; } }
        public GameState CurrentState { get { return _stateManager.CurrentState; } }
        public GameStateManager StateManager { get { return _stateManager; } }

        public Game1()
            : base()
        {
            Graphics = new GraphicsDeviceManager(this);
            
           


            Content.RootDirectory = "Content";

            Instance = this;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            _contentManager = new ContentResourceManager(this.Content, "GameSprites", "Fonts", "SoundFX", "Music", "UI");
            _input = new Input(this);
            _stateManager = new GameStateManager(this);
            _audio = new SFXManager(this);

            IsMouseVisible = true;


            Components.Add(_input);
            Components.Add(_stateManager);

            Window.Position = Point.Zero;
            Window.IsBorderless = true;

            Graphics.PreferredBackBufferWidth = GraphicsDevice.DisplayMode.Width;
            Graphics.PreferredBackBufferHeight = GraphicsDevice.DisplayMode.Height;
            Graphics.IsFullScreen = true;
            
            Graphics.ApplyChanges();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            ContentManager.LoadAllContent();


            background = ContentManager.Textures["bg1"];
            _audio.LoadContent();

            _mainMenu = new MainMenu(this, _stateManager);
            _gameplayScreen = new GameplayScreen(this, _stateManager);

            _stateManager.ChangeState(_mainMenu);

            base.LoadContent();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            _input.Dispose();
            _audio.Dispose();
            _stateManager.Dispose();
            _contentManager.UnloadAssets();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            _audio.Update();

            

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            SpriteBatch.Begin();
            SpriteBatch.Draw(background, new Rectangle(0, 0, background.Width, background.Height), Color.White);
            SpriteBatch.End();

            base.Draw(gameTime);

        }

        public void DrawBox(Texture2D texture, Rectangle bounds, int cornerSize, Color color)
        {
            // Corners
            SpriteBatch.Draw(texture, new Rectangle(bounds.Left, bounds.Top, cornerSize, cornerSize), new Rectangle(0, 0, cornerSize, cornerSize), color);
            SpriteBatch.Draw(texture, new Rectangle(bounds.Right - cornerSize, bounds.Top, cornerSize, cornerSize), new Rectangle(texture.Width - cornerSize, 0, cornerSize, cornerSize), color);
            SpriteBatch.Draw(texture, new Rectangle(bounds.Left, bounds.Bottom - cornerSize, cornerSize, cornerSize), new Rectangle(0, texture.Height - cornerSize, cornerSize, cornerSize), color);
            SpriteBatch.Draw(texture, new Rectangle(bounds.Right - cornerSize, bounds.Bottom - cornerSize, cornerSize, cornerSize), new Rectangle(texture.Width - cornerSize, texture.Height - cornerSize, cornerSize, cornerSize), color);

            // Content
            SpriteBatch.Draw(texture, new Rectangle(bounds.Left + cornerSize, bounds.Top + cornerSize, bounds.Width - cornerSize * 2, bounds.Height - cornerSize * 2), new Rectangle(cornerSize, cornerSize, texture.Width - cornerSize * 2, texture.Height - cornerSize * 2), color);

            // Border top / bottom
            SpriteBatch.Draw(texture, new Rectangle(bounds.Left + cornerSize, bounds.Top, bounds.Width - cornerSize * 2, cornerSize), new Rectangle(cornerSize, 0, texture.Width - cornerSize * 2, cornerSize), color);
            SpriteBatch.Draw(texture, new Rectangle(bounds.Left + cornerSize, bounds.Bottom - cornerSize, bounds.Width - cornerSize * 2, cornerSize), new Rectangle(cornerSize, texture.Height - cornerSize, texture.Width - cornerSize * 2, cornerSize), color);

            // Border left / right
            SpriteBatch.Draw(texture, new Rectangle(bounds.Left, bounds.Top + cornerSize, cornerSize, bounds.Height - cornerSize * 2), new Rectangle(0, cornerSize, cornerSize, texture.Height - cornerSize * 2), color);
            SpriteBatch.Draw(texture, new Rectangle(bounds.Right - cornerSize, bounds.Top + cornerSize, cornerSize, bounds.Height - cornerSize * 2), new Rectangle(texture.Width - cornerSize, cornerSize, cornerSize, texture.Height - cornerSize * 2), color);
        }
    }
}
