﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace StarTraders
{
    public class ContentResourceManager
    {
        private ContentManager _contentManager;

        string _texture2dpath = "";
        string _fontpath = "";
        string _sfxpath = "";
        string _songpath = "";
        string _uitextures = "";

        public Dictionary<string, Texture2D> Textures { get; protected set; }
        public Dictionary<string, SpriteFont> Fonts { get; protected set; }
        public Dictionary<string, SoundEffect> SoundFX { get; protected set; }
        public Dictionary<string, Song> Songs { get; protected set; }
        public Dictionary<string, Texture2D> UITextures { get; protected set; }

        public ContentResourceManager(ContentManager ContentManager, string Texture2Dpath, string SpriteFontPath, string SoundFXPath, string SongPath, string uitextures)
        {
            _contentManager = ContentManager;
            Textures = new Dictionary<string, Texture2D>();
            Fonts = new Dictionary<string, SpriteFont>();
            Songs = new Dictionary<string, Song>();
            SoundFX = new Dictionary<string, SoundEffect>();

            _texture2dpath = Texture2Dpath;
            _fontpath = SpriteFontPath;
            _sfxpath = SoundFXPath;
            _songpath = SongPath;
            _uitextures = uitextures;
        }

        public void LoadAllContent()
        {
            Textures = LoadContent<Texture2D>(_texture2dpath);
            Fonts = LoadContent<SpriteFont>(_fontpath);
            Songs = LoadContent<Song>(_songpath);
            SoundFX = LoadContent<SoundEffect>(_sfxpath);
            UITextures = LoadContent<Texture2D>(_uitextures);
        }

        private Dictionary<String, T> LoadContent<T>(string contentFolder)
        {
            //Load directory info, abort if none
            DirectoryInfo dir = new DirectoryInfo(_contentManager.RootDirectory + "\\" + contentFolder);
            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException();
            }

            //Init the resulting list
            Dictionary<String, T> result = new Dictionary<String, T>();

            //Load all files that matches the file filter
            FileInfo[] files = dir.GetFiles("*.*");
            foreach (FileInfo file in files)
            {
                string key = Path.GetFileNameWithoutExtension(file.Name);

                result[key] = _contentManager.Load<T>(contentFolder + "/" + key);
            }

            //Return the result
            return result;
        }

        /// <summary>
        /// Clear all assets.
        /// </summary>
        public void UnloadAssets()
        {
            Textures.Clear();
            Fonts.Clear();
            Songs.Clear();
            SoundFX.Clear();
            _contentManager.Unload();
        }
    }
}
