﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace StarTraders
{
    public enum GameplayState { Playing, Loss, Win, Pause }

    public class GameplayScreen : GameState
    {
        public static GameplayScreen Instance { get { return _instance; } }
        private static GameplayScreen _instance;

        private Game1 _game;
        private ContentResourceManager _contentManager;
        private GameManager _gameManager;
        private UIManager _ui;

        public GameManager GameManager { get { return _gameManager; } }
        public GameStateManager GameStateManager { get { return StateManager; } }
        public UIManager UI { get { return _ui; } }


        public GameplayScreen(Game1 game, GameStateManager statemanager)
            : base(game, statemanager)
        {
            _game = game;
            _contentManager = _game.ContentManager;
        }

        public override void Initialize()
        {
            _instance = this;
            _gameManager = new GameManager(_game);
            _ui = new UIManager(_game, this);

            _gameManager.Initialize();
            _gameManager.LoadContent();
            _ui.Initialize();
            _ui.LoadContent();

            _gameManager.StartGame();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            _gameManager.Update(gameTime);
            _ui.Update(gameTime);

            if(Input.GetKeyDown(Microsoft.Xna.Framework.Input.Keys.Escape))
            {
                if(!UIManager.MouseIsOverUI && GameManager.CurrentPlayState == GameplayState.Playing)
                {
                    GameManager.CurrentPlayState = GameplayState.Pause;
                }
                else if (GameManager.CurrentPlayState == GameplayState.Pause)
                {
                    GameManager.CurrentPlayState = GameplayState.Playing;
                }

            }
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            _gameManager.Draw(gameTime);

            _game.SpriteBatch.Begin();
            _ui.Draw(gameTime);
            _game.SpriteBatch.End();

            base.Draw(gameTime);
        }

        public void Reset()
        {
            _gameManager = null;
            _ui = null;
            IsInitialized = false;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

    }
}
