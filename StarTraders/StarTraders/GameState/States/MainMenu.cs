﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace StarTraders
{
    public class MainMenu : GameState
    {
        private Game1 _game;
        private ContentResourceManager _content;
        private GameStateManager _stateManager;

        private Texture2D _buttonBG;
        private SpriteFont _titfont;
        private SpriteFont _uifont;
        private int _buttonHeight = 18;
        private int _buttonWidth = 100;

        private Label _titleLabel;
        private Label _creditsLabel;
        private Button _startButton;
        private Button _exitButton;
        private Label _volumeLabel;

        private List<IControl> _controls;

        public MainMenu(Game1 game, GameStateManager statemanager)
            : base(game, statemanager)
        {
            _game = game;
            _content = _game.ContentManager;
            _stateManager = statemanager;
            _controls = new List<IControl>();
        }

        protected override void LoadContent()
        {
            _buttonBG = _content.UITextures["menu_button_bg"];
            _titfont = _content.Fonts["titlefont"];
            _uifont = _content.Fonts["uifont"];

            SetCreditsLabel();

            int lh = 26;

            _titleLabel = new Label(_game, _titfont, "Star Traders", new Point(_game.Graphics.PreferredBackBufferWidth / 2 - _buttonBG.Width / 2, 30));
            _startButton = new Button(_game, _buttonBG, new Point(_game.Graphics.PreferredBackBufferWidth / 2 - _buttonBG.Width / 2, _game.Graphics.PreferredBackBufferHeight / 2), _uifont, _buttonWidth, _buttonHeight);
            _exitButton = new Button(_game, _buttonBG, new Point(_game.Graphics.PreferredBackBufferWidth / 2 - _buttonBG.Width / 2, _game.Graphics.PreferredBackBufferHeight / 2 + lh), _uifont, _buttonWidth, _buttonHeight);
            _volumeLabel = new Label(_game, _uifont, "Volume: ", new Point(_game.Graphics.PreferredBackBufferWidth - (int)(_uifont.MeasureString("Volume: 999").X + 25), _game.Graphics.PreferredBackBufferHeight - 24));

            Action<Button> startgame = new Action<Button>(
                delegate 
                {
                    _game.GameplayScreen.Reset();
                    _stateManager.ChangeState(_game.GameplayScreen);
                    SFXManager.Instance.PlayEffect(SFXManager.Instance.ClickBlip1);
                });

            Action<Button> exitgame = new Action<Button>(
                delegate
                {
                    SFXManager.Instance.PlayEffect(SFXManager.Instance.ClickBlip1);
                    _game.Exit();
                });

            _startButton.D = startgame;
            _exitButton.D = exitgame;

            _startButton.SetText("Start Game");
            _exitButton.SetText("Exit Game");
            
            _controls.Add(_titleLabel);
            _controls.Add(_startButton);
            _controls.Add(_exitButton);
            _controls.Add(_volumeLabel);

            SFXManager.Instance.PlayMusic(SFXManager.Instance.MenuSong);

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            _volumeLabel.Text = "Volume: " + (Math.Round(MediaPlayer.Volume, 2) * 100).ToString();

            foreach (IControl c in _controls)
            {
                c.Update(gameTime);
            }

            _titleLabel.SetPosition(new Point(_game.Graphics.PreferredBackBufferWidth / 2 - Convert.ToInt32(_titleLabel.Font.MeasureString(_titleLabel.Text).X / 2 ), _game.Graphics.PreferredBackBufferHeight / 4));

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            _game.SpriteBatch.Begin();
            foreach (IControl c in _controls)
            {
                c.Draw();
            }
            _game.SpriteBatch.End();
            base.Draw(gameTime);
        }

        private void SetCreditsLabel()
        {
            _creditsLabel = new Label(_game, _uifont, "", new Point(20, 20));

            _creditsLabel.Text = "Music: Aaron \"Artuir\" Leibrick";
            _creditsLabel.Text += "\n\nProgramming: Jonathan \"ZXeno\" Cain";
            _creditsLabel.Text += "\n\nDesign: Jonathan \"ZXeno\" Cain";
            _creditsLabel.Text += "\n\nProgramming: Jonathan \"ZXeno\" Cain";
            _creditsLabel.Text += "\n\nSpawned from Ludum Dare 31, created with passion and ambition";
            _creditsLabel.Text += "\n(Probably too much ambition) in less than a month.";
            _creditsLabel.Text += "\n\nPlease visit our website for more information about this project!";
            _creditsLabel.Text += "\n\nHttp://www.signalreceived.com";

            _creditsLabel.SetPosition(new Point(20, _game.Graphics.PreferredBackBufferHeight - ((int)_uifont.MeasureString(_creditsLabel.Text).Y + 20)));

            _controls.Add(_creditsLabel);
        }
    }
}
