﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StarTraders
{
    public static class EventDispatcher
    {
        public static event EventHandler<Star> OpenStarWindow;
        public delegate void EventHandler<Star>(Star args);
        public static void OnOpenStarWindow(Star args)
        {
            if (OpenStarWindow != null)
            {
                OpenStarWindow(args);
            }
        }

    }
}
