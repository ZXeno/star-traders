﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace StarTraders
{
    internal class Image : IControl
    {
        Game1 _game;

        public Texture2D Sprite { get; set; }
        public Point Position { get; set; }
        public bool Enabled { get; set; }
        public Rectangle AnchoredRect { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int CornerSize { get; set; }
        public Rectangle Bounds
        {
            get
            {
                return new Rectangle(
                    Position.X + AnchoredRect.X,
                    Position.Y + AnchoredRect.Y,
                    Width,
                    Height
                    );
            }
        }

        public Image(Game1 game, Texture2D sprite, Point position)
        {
            _game = game;
            Sprite = sprite;
            Position = position;
            Width = sprite.Width;
            Height = sprite.Height;
            CornerSize = 0;
            Enabled = true;
        }

        public Image(Game1 game, Texture2D sprite, Point position, int width, int height)
        {
            _game = game;
            Sprite = sprite;
            Position = position;
            Width = width;
            Height = height;
            CornerSize = 0;
            Enabled = true;
        }

        public Image(Game1 game, Texture2D sprite, Point position, int width, int height, int corners)
        {
            _game = game;
            Sprite = sprite;
            Position = position;
            Width = width;
            Height = height;
            CornerSize = corners;
            Enabled = true;
        }

        public void Update(GameTime gameTime)
        {
            
        }

        public void Draw()
        {
            if (CornerSize > 0)
            {
                _game.DrawBox(Sprite, Bounds, CornerSize, Color.White);
            }
            else
            {
                _game.SpriteBatch.Draw(Sprite, Bounds, Color.White);
            }
        }

        public void HandleInput() { }

        public void SetPosition(Point p)
        {
            Position = p;
        }

        public bool HitTest()
        {
            if (Bounds.Contains(Input.MousePosition))
            {
                return true;
            }

            return false;
        }
    }
}
