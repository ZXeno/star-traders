﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace StarTraders
{
    public class UIManager
    {
        public static bool MouseIsOverUI { get; set; }

        private Game1 _game;
        private ContentResourceManager _content;
        private GameplayScreen _screen;

        private PauseAndLoss _pauseLossMenu;

        private Point _position;
        private int _uiWidth;
        private int _uiHeight;
        private Rectangle _anchorBox;

        
        private Texture2D _shipIcon;
        private Texture2D _markedTile;
        private Texture2D _darkGrayPix;
        private Texture2D _buttonBG;

        private SpriteFont _font;
        private List<IControl> _controls;

        private Label _playerMoney;
        private Label _selectedObjectInfo;
        private Label _shipWaitinglabel;
        private ListGroup _shipWaitingGroup;
        private Button _buyShipButton;
        
        public UIManager(Game1 game, GameplayScreen screen)
        {
            _game = game;
            _content = _game.ContentManager;
            _screen = screen;
            _uiWidth = _game.cUIResolutionW;
            _uiHeight = _game.cUIResolutionH;
        }

        public void Initialize()
        {
            _controls = new List<IControl>();
            _position = new Point(_game.cGameViewResolution, 0);
            _anchorBox = new Rectangle(_position.X, _position.Y, _uiWidth, _uiHeight);
        }

        public void LoadContent()
        {
            _font = _content.Fonts["uifont"];
            _shipIcon = _content.Textures["ship1"];
            _darkGrayPix = _content.UITextures["darkGrayPixel"];
            _markedTile = _content.Textures["markedTile"];
            _buttonBG = _content.UITextures["menu_button_bg"];

            _pauseLossMenu = new PauseAndLoss(_game, _font, _buttonBG, _screen);
            _pauseLossMenu.LoadContent();

            _playerMoney = new Label(_game, _font, "", new Point(_anchorBox.X + 1, _anchorBox.Y + 1));
            _selectedObjectInfo = new Label(_game, _font, "", new Point(_anchorBox.X + 1, _anchorBox.Y + 40));
            _shipWaitingGroup = new ListGroup(new Point(_anchorBox.X + 1, (_game.cUIResolutionH / 4) * 3), _uiWidth);
            _shipWaitingGroup.Clear();
            _shipWaitingGroup.ButtonTextAlignment = ButtonTextPosition.Right;
            _shipWaitinglabel = new Label(_game, _font, "Ships waiting for destinations:", new Point(_shipWaitingGroup.Position.X, _shipWaitingGroup.Position.Y - 20));

            _buyShipButton = new Button(_game, _shipIcon, new Point(_anchorBox.X + 2, _uiHeight - _shipIcon.Height - 8), _font);
            Action<Button> d = new Action<Button>(delegate 
                { 
                    GameManager.Instance.CreateShip(); 
                });
            _buyShipButton.D = d;
            _buyShipButton.Label.SetPosition(new Point(_buyShipButton.Position.X + _buyShipButton.Image.Width + 4, _buyShipButton.Position.Y));
            _buyShipButton.SetText("Buy ship: $5000");
            _buyShipButton.StretchBGImage = false;


            Button testb = new Button(_game, _shipIcon, new Point(50, 50), _font);
            testb.StretchBGImage = false;
            Window test = new Window(_game, _content.UITextures["WindowFrame"], _content.UITextures["CloseButton"], _font, new Point(16, 16),"TEST_WINDOW", 700, 400);
            test.Enabled = true;
            test.CanDrag = true;
            test.AddChildControl(testb);


            _controls.Add(_playerMoney);
            _controls.Add(_selectedObjectInfo);
            _controls.Add(_shipWaitinglabel);
            _controls.Add(_shipWaitingGroup);
            _controls.Add(_buyShipButton);
            _controls.Add(test);
        }

        public void Update(GameTime gameTime)
        {
            GamestatePlayUpdate(gameTime);

            _pauseLossMenu.Update();
        }

        public void Draw(GameTime gameTime)
        {
            _game.SpriteBatch.Draw(_darkGrayPix, _anchorBox, Color.White);
            foreach (IControl control in _controls)
            {
                control.Draw();
            }

            Ship s = Player.Current.SelectedEntity as Ship;
            if (s != null)
            {
                if (s.Path != null)
                {
                    foreach (Point n in s.Path)
                    {
                        Point pos = Map.Instance.GetTileAtPosition(n.X, n.Y).ScreenPosition;
                        _game.SpriteBatch.Draw(_markedTile, new Rectangle(pos.X, pos.Y, _markedTile.Width, _markedTile.Height), Color.White);
                    }
                }
            }

            _pauseLossMenu.Draw();
        }

        // Update during GameState.Play
        private void GamestatePlayUpdate(GameTime gameTime)
        {
            _playerMoney.Text = "Money: " + Player.Current.Money.ToString();

            SetSelectedEntityText();

            _shipWaitingGroup.Clear();
            if (Player.Current.ShipsNeedingDestination.Count > 0)
            {
                foreach (Ship s in Player.Current.ShipsNeedingDestination)
                {
                    Action<Button> selectShipAction = new Action<Button>(delegate { Player.Current.SelectedEntity = s; });
                    Button b = new Button(_game, _shipIcon, _shipWaitingGroup.Position, _font);
                    b.D = selectShipAction;
                    b.SetText(s.Name + " at " + s.MapPosition.ToString());
                    b.Label.SetPosition(new Point(b.Image.Bounds.Right + 1, b.Bounds.Y));
                    b.StretchBGImage = false;
                    _shipWaitingGroup.AddControl(b);
                }
                _shipWaitingGroup.Reposition();
            }



            foreach (IControl control in _controls)
            {
                control.Update(gameTime);
            }
        }

        private void SetSelectedEntityText()
        {
            if (Player.Current.SelectedEntity != null)
            {
                Entity entity = Player.Current.SelectedEntity;

                _selectedObjectInfo.Text = Player.Current.SelectedEntity.Name;

                if (entity != null && entity is Star)
                {
                    Star s_entity = entity as Star;
                    _selectedObjectInfo.Text += "\n Economy Type: " + s_entity.EconomyType.ToString();
                    _selectedObjectInfo.Text += "\n Goods:";
                    foreach (Good g in s_entity.MarketGoods)
                    {
                        _selectedObjectInfo.Text += "\n " + g.Name + ": $" + g.Price.ToString() +" qty: " + g.Quantity.ToString();
                    }
                }
                else if (entity != null && entity is Ship)
                {
                    Ship s_entity = entity as Ship;
                    _selectedObjectInfo.Text += "\n Cargo: " + s_entity.CurrentCargo.ToString() + " / " + s_entity.CargoCapacity.ToString();
                    _selectedObjectInfo.Text += "\n Fuel: " + s_entity.Fuel.ToString() + " / " + s_entity.MaxFuel.ToString();
                    _selectedObjectInfo.Text += "\n Current Location: " + s_entity.CurrentLocation.MapPosition.ToString();
                    if (s_entity.Destination != null && s_entity.Destination != s_entity.CurrentLocation)
                    {
                        _selectedObjectInfo.Text += "\n Current Destination: " + s_entity.Destination.MapPosition.ToString();
                    }

                    if (s_entity.PathQueue != null && s_entity.PathQueue.Count > 0)
                    {
                        foreach (Tile t in s_entity.PathQueue)
                        {
                            if (t.StarOnTile != null)
                            {
                                _selectedObjectInfo.Text += "\n Queued location: " + t.StarOnTile.Name + " - " + t.StarOnTile.MapPosition.ToString();
                            }
                            else
                            {
                                _selectedObjectInfo.Text += "\n Queued location: Tile " + t.MapPosition.ToString();
                            }
                        }
                    }
                }
                else
                {
                    _selectedObjectInfo.Text = "";
                }
            }
            else
            {
                _selectedObjectInfo.Text = "";
            }
        }
    }
}
