﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace StarTraders
{
    internal class Button : IControl
    {
        private Game1 _game;

        public Texture2D Image;
        public Label Label;
        public Point Position;
        public Point TextPosition = new Point(0, 0);
        public Action<Button> D;
        public Color HoverColor;
        public Color DefaultColor;
        private Color _currentColor;
        public Rectangle AnchoredRect { get; set; }
        public bool StretchBGImage = true;
        public bool Enabled { get; set; }
        public int CornerSize { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public bool FixedWidth { get; set; }

        // The bounds need to be calculatd off of a fixed width/height.
        // Width/height adjustments need to take place either in the update loop, or in the set position/set text/set size functions 
        // PLEASE FIX ME!
        public Rectangle Bounds
        {
            get
            {
                return new Rectangle(
                        Position.X + AnchoredRect.X,
                        Position.Y + AnchoredRect.Y,
                        Width,
                        Height
                        );
            }
        }

        public Button(Game1 game, Texture2D image, Point position, SpriteFont font)
        {
            _game = game;
            Image = image;
            Position = position;
            DefaultColor = Color.White;
            HoverColor = Color.White;
            _currentColor = DefaultColor;
            AnchoredRect = new Rectangle(0, 0, _game.Graphics.PreferredBackBufferWidth, _game.Graphics.PreferredBackBufferHeight);
            Width = Image.Width;
            Height = Image.Height;
            CornerSize = 4;
            Enabled = true;
            FixedWidth = false;

            Label = new Label(_game, font, "", Point.Zero);
            Label.AnchoredRect = this.Bounds;
        }

        public Button(Game1 game, Texture2D image, Point position, SpriteFont font, int width, int height)
        {
            _game = game;
            Image = image;
            Position = position;
            DefaultColor = Color.White;
            HoverColor = Color.White;
            _currentColor = DefaultColor;
            AnchoredRect = new Rectangle(0, 0, _game.Graphics.PreferredBackBufferWidth, _game.Graphics.PreferredBackBufferHeight);
            CornerSize = 4;
            Width = width;
            Height = height;
            Enabled = true;
            FixedWidth = true;

            Label = new Label(_game, font, "", Point.Zero);
            Label.AnchoredRect = this.Bounds;
        }

        public void SetText(string txt)
        {
            Label.Text = txt;

            if(!FixedWidth)
            {
                Width = (int)Label.Font.MeasureString(Label.Text).X + 4;
                Height = (int)Label.Font.MeasureString(Label.Text).Y + 4;

                Label.AnchoredRect = this.Bounds;

                Label.Position = new Point(Width / 2 - (int)Label.Font.MeasureString(Label.Text).X / 2, 2 + Height / 2 - (int)Label.Font.MeasureString(Label.Text).Y / 2);
            }
            else
            {
                Label.AnchoredRect = this.Bounds;

                Label.Position = new Point(Width / 2 - (int)Label.Font.MeasureString(Label.Text).X / 2, 2 + (Height / 2 - (int)Label.Font.MeasureString(Label.Text).Y / 2));
            }
        }

        public void SetSize(int w, int h)
        {
            Width = w;
            Height = h;

            if(Label.Text != "")
            {
                SetText(Label.Text);
            }
        }

        public void Update(GameTime gameTime)
        {
            if (Enabled)
            {
                Label.AnchoredRect = this.Bounds;

                if (Bounds.Contains(Input.MousePosition))
                {
                    _currentColor = HoverColor;
                }
                else
                {
                    _currentColor = DefaultColor;
                }

                if(Label.Text != "")
                {
                    SetText(Label.Text);
                }

                HandleInput();
            }
        }

        public void HandleInput()
        {
            // Perform assigned delegate action
            if (Input.GetButtonUp(0) && HitTest())
            {
                if (D != null)
                {
                    D(this);
                }
            }
        }

        public void Draw()
        {
            if (Enabled)
            {
                if (StretchBGImage)
                {
                    _game.DrawBox(Image, Bounds, CornerSize, _currentColor);
                }
                else
                {
                    _game.SpriteBatch.Draw(Image, new Rectangle(Position.X + AnchoredRect.X, Position.Y + AnchoredRect.Y, Image.Width, Image.Height), _currentColor);
                }

                Label.Draw();
            }
        }

        public void SetPosition(Point p)
        {
            Position = p;
        }

        public bool HitTest()
        {
            if (Bounds.Contains(Input.MousePosition))
            {
                return true;
            }

            return false;
        }
    }
}
