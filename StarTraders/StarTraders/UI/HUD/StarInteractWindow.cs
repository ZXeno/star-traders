﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace StarTraders
{
    internal class StarInteractWindow
    {
        private Game1 _game;
        private SpriteFont _font;
        private Texture2D _buttonBG;
        private Texture2D _buttonFrame;
        private Texture2D _buyBtnSprite;
        private Texture2D _sellBtnSprite;

        private Star _selectedStar;
        private Ship _selectedShip;

        private Window _starWindow;
        private Point _windowPos;
        private int _windowWidth = 600;
        private int _windowHeight = 460;

        private ListGroup _goodsColumn;
        private ListGroup _priceColumn;
        private ListGroup _quantityColumn;
        private ListGroup _transactButtonColumn;

        private Label _selectedShipLabel;
        private Label _buySellColumnLabel;
        private ListGroup _shipGoodsColumn;
        private ListGroup _shipQuantityColumn;

        private Label _upgradesLabel;
        private ListGroup _upgradesColumn;
        private ListGroup _upgradesButtonColumn;
        private Button _upgradeFuelButton;
        private Button _upgradeCargoButton;
        private Label _fuelupgradelabel;
        private Label _cargoupgradelabel;

        private Label _shiplistLabel;
        private Button _undockSelectedShipButton;
        private Button _refuelSelectedShipButton;

        private ListGroup _shipsDockedAtThisStar;
        private int _dockedWidth = 180;

        private Label _econText;

        public StarInteractWindow(Game1 game, SpriteFont font, Point position)
        {
            _game = game;
            _font = font;
            _windowPos = position;
            Events.DockOccurred += OnDockEvent;
        }

        public void Initialize()
        {
            // Build window
            _starWindow = new Window(
                _game,
                _game.ContentManager.UITextures["WindowFrame"],
                _game.ContentManager.UITextures["CloseButton"],
                _font,
                _windowPos,
                "",
                _windowWidth,
                _windowHeight
                );

            _buttonBG = _game.ContentManager.UITextures["WindowFrame"];
            _buttonFrame = _game.ContentManager.UITextures["WindowFrame"];
            _buyBtnSprite = _game.ContentManager.UITextures["BuyButton"];
            _sellBtnSprite = _game.ContentManager.UITextures["SellButton"];

            // Star economy type text label
            _econText = new Label(_game, _font, "", new Point(10, 25));

            // Star Info Columns
            _goodsColumn = new ListGroup(new Point(10, 70), 150);
            _priceColumn = new ListGroup(new Point(180, 70), 50);
            _quantityColumn = new ListGroup(new Point(260, 70), 50);

            // Ship Info and Transact Columns
            _selectedShipLabel = new Label(_game, _font, "", new Point(10, 170));
            _buySellColumnLabel = new Label(_game, _font, "Buy/Sell", new Point(330, 170));
            _shipGoodsColumn = new ListGroup(new Point(10, 190), 150);
            _shipQuantityColumn = new ListGroup(new Point(260, 190), 50);
            _transactButtonColumn = new ListGroup(new Point(330, 190), 50);
            _transactButtonColumn.Enabled = false;

            // Upgrades section
            _upgradesLabel = new Label(_game, _font, "Available upgrades for your ship:", new Point(10, 300));
            _upgradesLabel.Enabled = false;
            _upgradesColumn = new ListGroup(new Point(10, 320), 120);
            _upgradesButtonColumn = new ListGroup(new Point(330, 320), 32);
            _fuelupgradelabel = new Label(_game, _font, "FUEL_UPGRADE_INFO_TEXT", Point.Zero);
            _cargoupgradelabel = new Label(_game, _font, "CARGO_UPGRADE_INFO_TEXT", Point.Zero);

            // Upgrade buttons and delegates
            Action<Button> upgradefuelaction = new Action<Button>(
                delegate
                { 
                    if(Player.Current.Money > _selectedShip.FuelUpgradeCost)
                    {
                        if (_selectedShip.MaxFuel < _selectedShip.FuelUpgradeMax)
                        {
                            _selectedShip.BuyFuelTankUpgrade();
                            Player.Current.Money -= _selectedShip.FuelUpgradeCost;
                        }
                    }

                    SetShipData();
                });

            Action<Button> upgradecargoaction = new Action<Button>(
                delegate
                {
                    if (Player.Current.Money > _selectedShip.CargoUpgradeCost)
                    {
                        if (_selectedShip.CargoCapacity < _selectedShip.CargoUpgradeMax)
                        {
                            _selectedShip.BuyCargoUpgrade();
                            Player.Current.Money -= _selectedShip.CargoUpgradeCost;
                        }
                    }

                    SetShipData();
                });

            _upgradeFuelButton = new Button(_game, _game.ContentManager.UITextures["UpgradeButton"], Point.Zero, _font);
            _upgradeFuelButton.D = upgradefuelaction;
            _upgradeCargoButton = new Button(_game, _game.ContentManager.UITextures["UpgradeButton"], Point.Zero, _font);
            _upgradeCargoButton.D = upgradecargoaction;

            // add upgrade controls to their respective columns
            _upgradesColumn.AddControl(_cargoupgradelabel);
            _upgradesColumn.AddControl(_fuelupgradelabel);
            _upgradesButtonColumn.AddControl(_upgradeCargoButton);
            _upgradesButtonColumn.AddControl(_upgradeFuelButton);

            // Other ships in this system list
            _shiplistLabel = new Label(_game, _font, "Ships at this star:", new Point(360, 25));
            _shipsDockedAtThisStar = new ListGroup(new Point(360, 50), _dockedWidth);
            _shipsDockedAtThisStar.ButtonTextAlignment = ButtonTextPosition.Right;

            // Undock button
            _undockSelectedShipButton = new Button(_game, _game.ContentManager.UITextures["WindowFrame"], new Point(_starWindow.Bounds.Width, _starWindow.Bounds.Height), _font);
            _undockSelectedShipButton.SetText("Dock/Undock Selected Ship");
            Action<Button> undockaction = new Action<Button>(
                delegate
                {
                    if(_selectedShip != null)
                    {
                        if (_selectedShip.IsDocked)
                        {
                            Events.OnShipUnDock(_selectedStar, _selectedShip);

                            if (_selectedShip.PathQueue.Count == 0)
                            {
                                if (Player.Current.SelectedEntity != _selectedShip)
                                {
                                    Player.Current.SelectedEntity = _selectedShip;
                                }

                                CloseWindow();
                            }
                            else
                            {
                                UpdateDockedShips();
                                SetShipData();
                            }
                        }
                        else
                        {
                            _selectedShip.IsDocked = true;
                            Events.OnShipDock(_selectedStar, _selectedShip);
                            SetShipData();
                            UpdateDockedShips();
                        }
                    }
                });
            _undockSelectedShipButton.D = undockaction;
            _undockSelectedShipButton.SetPosition(new Point(_undockSelectedShipButton.Position.X - (_undockSelectedShipButton.Width + 2), _undockSelectedShipButton.Position.Y - (_undockSelectedShipButton.Height + 2)));
            _undockSelectedShipButton.Enabled = false;

            // refuel button
            _refuelSelectedShipButton = new Button(_game, _game.ContentManager.UITextures["WindowFrame"], new Point(_undockSelectedShipButton.Position.X, _starWindow.Bounds.Height), _font);
            _refuelSelectedShipButton.SetText("Refuel Ship");
            Action<Button> refuelaction = new Action<Button>(
                delegate
                {
                    if (_selectedShip != null)
                    {
                        if (_selectedShip.IsDocked)
                        {
                            _selectedShip.Refuel(_selectedStar);
                        }

                        SetShipData();
                    }
                });
            _refuelSelectedShipButton.D = refuelaction;
            _refuelSelectedShipButton.Enabled = false;
            _refuelSelectedShipButton.SetPosition(new Point(_refuelSelectedShipButton.Position.X - (_refuelSelectedShipButton.Width + 2), _refuelSelectedShipButton.Position.Y - (_refuelSelectedShipButton.Height + 2)));



            // Add all controls to the window
            _starWindow.AddChildControl(_goodsColumn);
            _starWindow.AddChildControl(_priceColumn);
            _starWindow.AddChildControl(_buySellColumnLabel);
            _starWindow.AddChildControl(_transactButtonColumn);
            _starWindow.AddChildControl(_quantityColumn);
            _starWindow.AddChildControl(_econText);
            _starWindow.AddChildControl(_selectedShipLabel);
            _starWindow.AddChildControl(_shipGoodsColumn);
            _starWindow.AddChildControl(_shipQuantityColumn);
            _starWindow.AddChildControl(_shiplistLabel);
            _starWindow.AddChildControl(_shipsDockedAtThisStar);
            _starWindow.AddChildControl(_undockSelectedShipButton);
            _starWindow.AddChildControl(_refuelSelectedShipButton);
            _starWindow.AddChildControl(_upgradesLabel);
            _starWindow.AddChildControl(_upgradesColumn);
            _starWindow.AddChildControl(_upgradesButtonColumn);

            // Hide window, set draggable
            _starWindow.Enabled = false;
            _starWindow.CanDrag = false;

            // prescribe to events
            Events.OpenStarWindow += OpenWindow;
        }

        public void Update(GameTime gameTime)
        {

            if (_starWindow.Enabled && UIManager.MouseIsOverUI && _starWindow.HitTest() && Input.GetKeyDown(Microsoft.Xna.Framework.Input.Keys.Escape))
            {
                CloseWindow();
            }

            if (_starWindow.Enabled)
            {
                if(_selectedShip != null)
                {
                    SetToggledControlsEnabled(_selectedShip.IsDocked);
                }

                if(_selectedStar.ShipsInSystem.Count > 0)
                {
                    _undockSelectedShipButton.Enabled = true;
                }
                else
                {
                    _undockSelectedShipButton.Enabled = false;
                }

                _starWindow.Update(gameTime);
                if(_starWindow.IsHovered)
                {
                    UIManager.MouseIsOverUI = true;
                }
            }
        }

        public void Draw(GameTime gameTime)
        {
            if (_starWindow.Enabled)
            {
                _starWindow.Draw();
            }
        }

        public void OpenWindow(Star star)
        {
            if(_selectedStar != null)
            {
                ClearData();
            }

            if(_transactButtonColumn.Controls.Count <= 0)
            {
                foreach (Good g in star.MarketGoods)
                {
                    // buy action
                    Action<Button> buyaction = new Action<Button>(
                        delegate 
                        {
                            Good sg = _selectedShip.CargoHold.Goods.Find(x => x.Name == g.Name);

                            if (g.Quantity > 1 && Player.Current.Money > g.Price && sg.Quantity < _selectedShip.CargoCapacity)
                            {
                                g.Quantity--;
                                sg.Quantity++;
                                Player.Current.Money -= g.Price;
                                UpdateShipQuantities();
                                UpdateStarQuantities();
                            }
                        });

                    // sell action
                    Action<Button> sellaction = new Action<Button>(
                        delegate
                        {
                            Good sg = _selectedShip.CargoHold.Goods.Find(x => x.Name == g.Name);

                            if (sg.Quantity > 0)
                            {
                                g.Quantity++;
                                sg.Quantity--;
                                Player.Current.Money += g.Price;
                                UpdateShipQuantities();
                                UpdateStarQuantities();
                            }
                        });

                    // buy button
                    Button bb = new Button(_game, _buyBtnSprite, new Point(0, 0), _font, 16, 16);
                    bb.AnchoredRect = _transactButtonColumn.Bounds;
                    bb.D = buyaction;

                    // Sell button
                    Button sb = new Button(_game, _sellBtnSprite, new Point(0, 0), _font, 16, 16);
                    sb.AnchoredRect = _transactButtonColumn.Bounds;
                    sb.D = sellaction;

                    // create row
                    IControl[] buttons = new IControl[] { bb, sb };
                    ListGroupRow btnrow = new ListGroupRow(32, 20, buttons);

                    // add to buttons group
                    _transactButtonColumn.AddControl(btnrow);
                }

                if(_selectedShip == null || !_selectedShip.IsDocked)
                {
                    SetToggledControlsEnabled(false);
                }
            }

            _selectedStar = star;
            SetData(_selectedStar);
            _starWindow.Enabled = true;
        }
 
        public void CloseWindow()
        {
            _starWindow.Enabled = false;

            ClearData();
        }

        private void SetData(Star star)
        {
            _starWindow.SetWindowTitle(star.Name);
            _econText.Text = "Economy type: " + star.EconomyType.ToString() + " - " + Market.GetMarketLabel(star.EconomyType);

            UpdateStarQuantities();

            if(star.ShipsInSystem.Count > 0)
            {
                if (_selectedShip == null)
                {
                    _selectedShip = star.ShipsInSystem[0];
                    SetSelectedShipControls();
                }

                SetShipData();
            }
            else
            {
                SetSelectedShipControls();
                SetToggledControlsEnabled(false);
            }

            UpdateDockedShips();
        }

        private void UpdateStarQuantities()
        {
            _goodsColumn.Clear();
            _priceColumn.Clear();
            _quantityColumn.Clear();

            foreach (Good good in _selectedStar.MarketGoods)
            {
                Label nl = new Label(_game, _font, good.Name, new Point(0, 0));
                Label pl = new Label(_game, _font, "$" + good.Price.ToString(), new Point(0, 0));
                Label ql = new Label(_game, _font, "Qty: " + good.Quantity.ToString(), new Point(0, 0));

                nl.AnchoredRect = _goodsColumn.Bounds;
                pl.AnchoredRect = _priceColumn.Bounds;
                ql.AnchoredRect = _quantityColumn.Bounds;

                _goodsColumn.AddControl(nl);
                _priceColumn.AddControl(pl);
                _quantityColumn.AddControl(ql);
            }
        }

        public void SetShipData()
        {
            UpdateShipQuantities();
            SetSelectedShipControls();
            SetToggledControlsEnabled(_selectedShip.IsDocked);
        }

        private void UpdateShipQuantities()
        {
            _shipGoodsColumn.Clear();
            _shipQuantityColumn.Clear();
            
            foreach (Good g in _selectedShip.CargoHold.Goods)
            {
                Label gl = new Label(_game, _font, g.Name, new Point(0, 0));
                Label ql = new Label(_game, _font, "Qty: " + g.Quantity.ToString(), new Point(0, 0));

                gl.AnchoredRect = _shipGoodsColumn.Bounds;
                ql.AnchoredRect = _shipQuantityColumn.Bounds;

                _shipGoodsColumn.AddControl(gl);
                _shipQuantityColumn.AddControl(ql);
            }
        }

        private void UpdateDockedShips()
        {
            _shipsDockedAtThisStar.Clear();
            foreach (Ship s in _selectedStar.ShipsInSystem)
            {
                Action<Button> selectShipAction = new Action<Button>(
                    delegate 
                    { 
                        _selectedShip = s;
                        SetSelectedShipControls();
                    });

                Button b = new Button(_game, _buttonBG, Point.Zero, _font, _shipsDockedAtThisStar.Width, _shipsDockedAtThisStar.LineHeight);
                b.AnchoredRect = _shipsDockedAtThisStar.Bounds;
                b.D = selectShipAction;
                if (s.IsDocked) { b.SetText(s.Name + " - Docked"); }
                else { b.SetText(s.Name + " - Not Docked"); }
                //b.StretchBGImage = false;
                b.FixedWidth = true;
                _shipsDockedAtThisStar.AddControl(b);
            }
        }

        private void SetToggledControlsEnabled(bool dockedstatus)
        {
            _transactButtonColumn.Enabled = dockedstatus;
            _buySellColumnLabel.Enabled = dockedstatus;
            _upgradesLabel.Enabled = dockedstatus;
            _refuelSelectedShipButton.Enabled = dockedstatus;
            _upgradesButtonColumn.Enabled = dockedstatus;
            _upgradesColumn.Enabled = dockedstatus;
        }

        public void OnDockEvent(Star star, Ship ship)
        {
            if (star == _selectedStar)
            {
                if(_selectedShip != ship)
                {
                    if(_selectedShip == null)
                    {
                        _selectedShip = ship;
                    }
                }

                SetShipData();
                SetToggledControlsEnabled(true);

                UpdateDockedShips();
            }
        }

        public void OnUndockEvent(Star star, Ship ship)
        {
            if(star == _selectedStar)
            {
                if(ship == _selectedShip)
                {
                    SetSelectedShipControls();
                    SetToggledControlsEnabled(true);
                }
            }
        }

        private void ClearData()
        {
            _starWindow.SetWindowTitle("");
            _goodsColumn.Clear();
            _priceColumn.Clear();
            _quantityColumn.Clear();
            _shipGoodsColumn.Clear();
            _shipQuantityColumn.Clear();
            _shipsDockedAtThisStar.Clear();
            _transactButtonColumn.Clear();
            SetToggledControlsEnabled(false);
            
            _selectedStar = null;
            _selectedShip = null;
        }

        private void SetSelectedShipControls()
        {
            if (_selectedShip != null)
            {
                // ship status label
                string selectedlabeltext = _selectedShip.Name + " - Status ";
                if (_selectedShip.IsDocked) { selectedlabeltext += "Docked - Fuel: " + _selectedShip.Fuel.ToString() + "/" + _selectedShip.MaxFuel.ToString(); }
                else { selectedlabeltext += "Not Docked - Fuel: " + _selectedShip.Fuel.ToString() + "/" + _selectedShip.MaxFuel.ToString(); }
                _selectedShipLabel.Text = selectedlabeltext;

                // refuel button
                int diff = _selectedShip.MaxFuel - _selectedShip.Fuel;
                float cost = (float)Math.Round((((_selectedStar.MarketGoods.Find(g => g.Name == "Fuel").Price) * diff) * .1f), 2);
                _refuelSelectedShipButton.SetText("Refuel Ship: $"+ cost.ToString());
                _refuelSelectedShipButton.SetPosition(new Point(_undockSelectedShipButton.Position.X - (_refuelSelectedShipButton.Width + 2), _starWindow.Bounds.Height - (_refuelSelectedShipButton.Height + 2)));

                // upgrades
                if (_selectedShip.MaxFuel < _selectedShip.FuelUpgradeMax)
                {
                    _fuelupgradelabel.Text = "Upgrade Fuel Cap. " + _selectedShip.MaxFuel.ToString() + "/" + _selectedShip.FuelUpgradeMax.ToString() + " ($" + _selectedShip.FuelUpgradeCost.ToString() + ")";
                    _upgradeFuelButton.Enabled = true;
                }
                else 
                {
                    _fuelupgradelabel.Text = "Upgrade Fuel Cap. " + _selectedShip.MaxFuel.ToString() + "/" + _selectedShip.FuelUpgradeMax.ToString() + " ( MAXED! )";
                    _upgradeFuelButton.Enabled = false;
                }

                if (_selectedShip.CargoCapacity < _selectedShip.CargoUpgradeMax)
                {
                    _cargoupgradelabel.Text = "Upgrade Cargo Cap. " + _selectedShip.CargoCapacity.ToString() + "/" + _selectedShip.CargoUpgradeMax.ToString() + " ($" + _selectedShip.CargoUpgradeCost.ToString() + ")";
                    _upgradeCargoButton.Enabled = true;
                }
                else
                {
                    _cargoupgradelabel.Text = "Upgrade Cargo Cap. " + _selectedShip.CargoCapacity.ToString() + "/" + _selectedShip.CargoUpgradeMax.ToString() + " ( MAXED! )";
                    _upgradeCargoButton.Enabled = false;
                }
            }
            else
            {
                _selectedShipLabel.Text = "";
            }
        }

    }
}
