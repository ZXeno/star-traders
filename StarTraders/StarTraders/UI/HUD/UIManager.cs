﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace StarTraders
{
    public class UIManager
    {
        public static bool MouseIsOverUI { get; set; }

        private Game1 _game;
        private ContentResourceManager _content;
        private GameplayScreen _screen;

        private PauseAndLoss _pauseLossMenu;
        private StarInteractWindow _starWindow;
        private RightPanel _rightPane;
        
        private Texture2D _shipIcon;
        private Texture2D _markedTile;
        private Texture2D _darkGrayPix;
        private Texture2D _buttonBG;

        private SpriteFont _font;
        private List<IControl> _controls;


        public UIManager(Game1 game, GameplayScreen screen)
        {
            _game = game;
            _content = _game.ContentManager;
            _screen = screen;
        }

        public void Initialize()
        {
            _controls = new List<IControl>();

        }

        public void LoadContent()
        {
            _font = _content.Fonts["uifont"];
            _shipIcon = _content.Textures["ship1"];
            _darkGrayPix = _content.UITextures["darkGrayPixel"];
            _markedTile = _content.Textures["markedTile"];
            _buttonBG = _content.UITextures["menu_button_bg"];

            _pauseLossMenu = new PauseAndLoss(_game, _font, _buttonBG, _screen);
            _pauseLossMenu.LoadContent();

            _starWindow = new StarInteractWindow(_game, _font, new Point(_game.Graphics.PreferredBackBufferWidth / 2 - (450 / 2), _game.Graphics.PreferredBackBufferHeight / 2 - (350 / 2)));
            _starWindow.Initialize();

            _rightPane = new RightPanel(_game, _font, _game.cUIResolutionW, _game.cUIResolutionH, new Point(_game.Graphics.PreferredBackBufferWidth - _game.cUIResolutionW, 0));
            _rightPane.Initialize();
        }

        public void Update(GameTime gameTime)
        {
            GameplayState currstate = GameManager.Instance.CurrentPlayState;

            if (currstate == GameplayState.Playing)
            {
                GamestatePlayUpdate(gameTime);
            }

            if (currstate == GameplayState.Loss || currstate == GameplayState.Win || currstate == GameplayState.Pause)
            {
                _pauseLossMenu.Update(gameTime);
            }
        }

        public void Draw(GameTime gameTime)
        {
            _game.SpriteBatch.End();

            _game.SpriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, GameManager.Instance.Camera.GetTransformation(_game.GraphicsDevice));
            Ship s = Player.Current.SelectedEntity as Ship;
            if (s != null)
            {
                if (s.Path != null)
                {
                    foreach (Point n in s.Path)
                    {
                        Point pos = Map.Instance.GetTileAtPosition(n.X, n.Y).ScreenPosition;
                        _game.SpriteBatch.Draw(_markedTile, new Rectangle(pos.X, pos.Y, _markedTile.Width, _markedTile.Height), Color.White);
                    }
                }
            }
            _game.SpriteBatch.End();

            _game.SpriteBatch.Begin();

            foreach (IControl control in _controls)
            {
                control.Draw();
            }

            _rightPane.Draw(gameTime);
            _starWindow.Draw(gameTime);
            _pauseLossMenu.Draw();
            //_game.SpriteBatch.DrawString(_font, "Mouse is over UI: " + MouseIsOverUI.ToString(), new Vector2(10, 10), Color.White);

            
        }

        // Update during GameState.Play
        private void GamestatePlayUpdate(GameTime gameTime)
        {
            MouseIsOverUI = false;

            _starWindow.Update(gameTime);
            _rightPane.Update(gameTime);
            
            foreach (IControl control in _controls)
            {
                if (control.Enabled)
                {
                    control.Update(gameTime);

                    if (!MouseIsOverUI && control.HitTest())
                    {
                        MouseIsOverUI = true;
                    }
                }
            }

            if (!MouseIsOverUI && (Input.GetButton(0) || Input.GetButton(1) || Input.GetKeyDown(Microsoft.Xna.Framework.Input.Keys.Escape)))
            {
                _starWindow.CloseWindow();
            }
        }

        
    }
}
