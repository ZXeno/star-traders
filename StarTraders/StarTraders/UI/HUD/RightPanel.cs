﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace StarTraders
{
    internal class RightPanel
    {

        

        private Game1 _game;
        private SpriteFont _font;
        private Point _position;
        private int _uiWidth;
        private int _uiHeight;
        private Rectangle _anchorBox;

        private Label _playerMoney;
        private Label _selectedObjectInfo;
        private Label _shipWaitinglabel;
        private ListGroup _shipWaitingGroup;
        private Button _buyShipButton;
        private Button _openStarWindowButton;

        private Texture2D _buttonFrame;
        private Texture2D _markedTile;
        private Texture2D _darkGrayPix;
        private Texture2D _buttonBG;

        private List<IControl> _controls;

        

        public RightPanel(Game1 game, SpriteFont font, int width, int height, Point position)
        {
            _game = game;
            _font = font;
            _uiHeight = height;
            _uiWidth = width;
            _position = position;
            _anchorBox = new Rectangle(_position.X, _position.Y, _uiWidth, _uiHeight);
            _controls = new List<IControl>();
        }

        public void LoadContent()
        {
            _font = _game.ContentManager.Fonts["uifont"];
            _buttonFrame = _game.ContentManager.UITextures["WindowFrame"];
            _darkGrayPix = _game.ContentManager.UITextures["darkGrayPixel"];
            _markedTile = _game.ContentManager.Textures["markedTile"];
            _buttonBG = _game.ContentManager.UITextures["menu_button_bg"];
        }

        public void Initialize()
        {
            LoadContent();

            _playerMoney = new Label(_game, _font, "", new Point(_anchorBox.X + 1, _anchorBox.Y + 1));

            _openStarWindowButton = new Button(_game, _game.ContentManager.UITextures["WindowFrame"], new Point(_anchorBox.X + 1, _anchorBox.Y + 30), _font);
            _openStarWindowButton.StretchBGImage = true;
            _openStarWindowButton.SetText("Open System View");
            Action<Button> b = new Action<Button>(
                delegate
                {
                    if (Player.Current.SelectedTile != null && Player.Current.SelectedTile.StarOnTile != null)
                    {
                        Events.OnOpenStarWindow(Player.Current.SelectedTile.StarOnTile);
                    }
                    else if (Player.Current.SelectedEntity != null)
                    {
                        if(Player.Current.SelectedEntity is Star)
                        {
                            Events.OnOpenStarWindow(Player.Current.SelectedEntity as Star);
                        }
                        else if (Player.Current.SelectedEntity is Ship)
                        {
                            Events.OnOpenStarWindow((Player.Current.SelectedEntity as Ship).Destination.StarOnTile);
                        }
                    }
                });
            _openStarWindowButton.D = b;
            _openStarWindowButton.Enabled = false;

            _selectedObjectInfo = new Label(_game, _font, "", new Point(_anchorBox.X + 1, _anchorBox.Y + 60));
            _shipWaitingGroup = new ListGroup(new Point(_anchorBox.X + 1, (_game.cUIResolutionH / 4) * 3), _uiWidth);
            _shipWaitingGroup.Clear();
            _shipWaitingGroup.ButtonTextAlignment = ButtonTextPosition.Right;
            _shipWaitinglabel = new Label(_game, _font, "Ships waiting for destinations:", new Point(_shipWaitingGroup.Position.X, _shipWaitingGroup.Position.Y - 20));

            _buyShipButton = new Button(_game, _buttonFrame, new Point(_anchorBox.X, _uiHeight - 24 - 8), _font);
            Action<Button> d = new Action<Button>(
                delegate
                {
                    GameManager.Instance.BuyShip();
                });

            _buyShipButton.D = d;
            _buyShipButton.Label.SetPosition(new Point(_buyShipButton.Position.X + _buyShipButton.Image.Width + 4, _buyShipButton.Position.Y));
            _buyShipButton.SetText("Buy ship: $5000");
            _buyShipButton.StretchBGImage = true;

            _controls.Add(_playerMoney);
            _controls.Add(_selectedObjectInfo);
            _controls.Add(_shipWaitinglabel);
            _controls.Add(_shipWaitingGroup);
            _controls.Add(_buyShipButton);
            _controls.Add(_openStarWindowButton);
        }

        public void Update(GameTime gameTime)
        {
            _playerMoney.Text = "Money: " + Player.Current.Money.ToString();

            if (Player.Current.SelectedTile != null)
            {
                if (Player.Current.SelectedTile.StarOnTile != null && !_openStarWindowButton.Enabled)
                {
                    _openStarWindowButton.Enabled = true;
                }
                else if (Player.Current.SelectedTile.StarOnTile == null && _openStarWindowButton.Enabled)
                {
                    _openStarWindowButton.Enabled = false;
                }
            }
            else if (Player.Current.SelectedEntity is Star)
            {
                _openStarWindowButton.Enabled = true;
            }
            else
            {
                _openStarWindowButton.Enabled = false;
            }

            SetSelectedEntityText();

            _shipWaitingGroup.Clear();
            if (Player.Current.ShipsNeedingDestination.Count > 0)
            {
                foreach (Ship s in Player.Current.ShipsNeedingDestination)
                {
                    Action<Button> selectShipAction = new Action<Button>(delegate { Player.Current.SelectedEntity = s; });

                    Button b = new Button(_game, _buttonFrame, Point.Zero, _font, _shipWaitingGroup.Width - 6, 20);
                    b.AnchoredRect = _shipWaitingGroup.Bounds;
                    b.D = selectShipAction;
                    b.SetText(s.Name + " at " + "(" + s.MapPosition.X.ToString() + ", " + s.MapPosition.Y.ToString() + ")");
                    b.FixedWidth = true;
                    _shipWaitingGroup.AddControl(b);
                }
                _shipWaitingGroup.Reposition();
            }

            foreach (IControl control in _controls)
            {
                control.Update(gameTime);

                if (control.Enabled && control.HitTest())
                {
                    UIManager.MouseIsOverUI = true;
                }
            }
        }

        public void Draw(GameTime gameTime)
        {
            _game.SpriteBatch.Draw(_darkGrayPix, _anchorBox, Color.White);

            foreach (IControl control in _controls)
            {
                control.Draw();
            }
        }

        private void SetSelectedEntityText()
        {
            if (Player.Current.SelectedEntity != null)
            {
                Entity entity = Player.Current.SelectedEntity;

                _selectedObjectInfo.Text = entity.Name;

                if(entity != null && entity is Star)
                {
                    Star starent = entity as Star;
                    _selectedObjectInfo.Text += "\nEconomy type: " + starent.EconomyType.ToString() + " \n" + Market.GetMarketLabel(starent.EconomyType);
                }
                else if (entity != null && entity is Ship)
                {
                    Ship s_entity = entity as Ship;
                    _selectedObjectInfo.Text += "\nFuel: " + s_entity.Fuel.ToString() + " / " + s_entity.MaxFuel.ToString();
                    _selectedObjectInfo.Text += "\nCurrent Location: " + s_entity.CurrentLocation.MapPosition.ToString();
                    if (s_entity.Destination != null && s_entity.Destination != s_entity.CurrentLocation)
                    {
                        _selectedObjectInfo.Text += "\nCurrent Destination: " + s_entity.Destination.MapPosition.ToString();
                    }

                    if (s_entity.PathQueue != null && s_entity.PathQueue.Count > 0)
                    {
                        foreach (Tile t in s_entity.PathQueue)
                        {
                            if (t.StarOnTile != null)
                            {
                                _selectedObjectInfo.Text += "\nQueued location: " + t.StarOnTile.Name + " - " + t.StarOnTile.MapPosition.ToString();
                            }
                            else
                            {
                                _selectedObjectInfo.Text += "\nQueued location: Tile " + t.MapPosition.ToString();
                            }
                        }
                    }
                }
                else
                {
                    _selectedObjectInfo.Text = "";
                }
            }
            else
            {
                _selectedObjectInfo.Text = "";
            }
        }
    }
}
