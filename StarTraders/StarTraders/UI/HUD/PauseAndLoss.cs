﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace StarTraders
{
    class PauseAndLoss
    {
        private Game1 _game;
        private GameplayScreen _screen;

        private Texture2D _buttonBG;
        private SpriteFont _font;

        private ListGroup _pauseMenuGroup;
        private ListGroup _lossMenuGroup;

        private Button _resumeButton;
        private Button _pexitButton;
        private Button _pmainMenu;
        private Button _lexitButton;
        private Button _lmainMenu;
        private Label _menuLabel;

        private int _buttonHeight = 20;
        private int _buttonWidth = 125;
        
        private List<IControl> _ctrlList;

        public PauseAndLoss(Game1 game, SpriteFont font, Texture2D buttonbg, GameplayScreen screen)
        {
            _game = game;
            _ctrlList = new List<IControl>();
            _font = font;
            _buttonBG = buttonbg;
            _screen = screen;
        }

        public void LoadContent()
        {
            _pauseMenuGroup = new ListGroup(new Point(_game.Graphics.PreferredBackBufferWidth / 2 - _buttonWidth / 2, _game.Graphics.PreferredBackBufferHeight / 2), _buttonWidth + 4);
            _lossMenuGroup = new ListGroup(new Point(_game.Graphics.PreferredBackBufferWidth / 2 - _buttonWidth / 2, _game.Graphics.PreferredBackBufferHeight / 2), _buttonWidth + 4);
            _menuLabel = new Label(_game, _game.ContentManager.Fonts["titlefont"], "", new Point(_game.Graphics.PreferredBackBufferWidth / 2 - 125, _game.Graphics.PreferredBackBufferHeight / 4));

            _resumeButton = new Button(_game, _buttonBG, Point.Zero, _font, _buttonWidth, _buttonHeight);
            _pexitButton = new Button(_game, _buttonBG, Point.Zero, _font, _buttonWidth, _buttonHeight);
            _pmainMenu = new Button(_game, _buttonBG, Point.Zero, _font, _buttonWidth, _buttonHeight);
            _lexitButton = new Button(_game, _buttonBG, Point.Zero, _font, _buttonWidth, _buttonHeight);
            _lmainMenu = new Button(_game, _buttonBG, Point.Zero, _font, _buttonWidth, _buttonHeight);

            _resumeButton.AnchoredRect = _pauseMenuGroup.Bounds;
            _pexitButton.AnchoredRect = _pauseMenuGroup.Bounds;
            _pmainMenu.AnchoredRect = _pauseMenuGroup.Bounds;
            _lexitButton.AnchoredRect = _lossMenuGroup.Bounds;
            _lmainMenu.AnchoredRect = _lossMenuGroup.Bounds;

            Action<Button> resume = new Action<Button>(delegate { GameManager.Instance.CurrentPlayState = GameplayState.Playing; });
            Action<Button> exit = new Action<Button>(delegate { _game.Exit(); });
            Action<Button> menu = new Action<Button>(
                delegate 
                {
                    _game.GameplayScreen.Reset();
                    _screen.GameStateManager.ChangeState(_game.MainMenu); 
                });

            _resumeButton.D = resume;
            _pexitButton.D = exit;
            _pmainMenu.D = menu;
            
            _lexitButton.D = exit;
            _lmainMenu.D = menu;

            _resumeButton.SetText("RESUME GAME");
            _pexitButton.SetText("EXIT GAME");
            _pmainMenu.SetText("MAIN MENU");
            
            _lexitButton.SetText("EXIT GAME");
            _lmainMenu.SetText("MAIN MENU");

            _pauseMenuGroup.LineHeight = _buttonHeight + 6;
            _pauseMenuGroup.AddControl(_resumeButton);
            _pauseMenuGroup.AddControl(_pmainMenu);
            _pauseMenuGroup.AddControl(_pexitButton);

            _lossMenuGroup.LineHeight = _buttonHeight + 6;
            _lossMenuGroup.AddControl(_lmainMenu);
            _lossMenuGroup.AddControl(_lexitButton);

            _ctrlList.Add(_pauseMenuGroup);
            _ctrlList.Add(_lossMenuGroup);
        }

        public void Update(GameTime gameTime)
        {
            GameplayState gps = GameManager.Instance.CurrentPlayState;

            if (gps == GameplayState.Loss || gps == GameplayState.Win || gps == GameplayState.Pause)
            {
                if (gps == GameplayState.Loss || gps == GameplayState.Win)
                {
                    _menuLabel.Text = GameManager.Instance.EndGameConditionText;
                    _lossMenuGroup.Update(gameTime);
                }

                if (gps == GameplayState.Pause)
                {
                    _menuLabel.Text = "PAUSED";
                    _pauseMenuGroup.Update(gameTime);
                }

                _menuLabel.SetPosition(new Point(_game.Graphics.PreferredBackBufferWidth / 2 - Convert.ToInt32(_menuLabel.Font.MeasureString(_menuLabel.Text).X / 2), _game.Graphics.PreferredBackBufferHeight / 4));
            }
        }

        public void Draw()
        {
            GameplayState gps = GameManager.Instance.CurrentPlayState;

            if (gps == GameplayState.Loss || gps == GameplayState.Win || gps == GameplayState.Pause)
            {
                _menuLabel.Draw();
                if (gps == GameplayState.Loss || gps == GameplayState.Win)
                {
                    _lossMenuGroup.Draw();
                }


                if (gps == GameplayState.Pause)
                {
                    _pauseMenuGroup.Draw();
                }
            }
        }
    }
}
