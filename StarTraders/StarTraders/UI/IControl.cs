﻿using System;
using Microsoft.Xna.Framework;

namespace StarTraders
{
    public interface IControl
    {
        bool Enabled { get; set; }
        Rectangle AnchoredRect { get; set; }

        void Update(GameTime gameTime);
        void Draw();
        void HandleInput();
        void SetPosition(Point p);
        bool HitTest();
    }
}
