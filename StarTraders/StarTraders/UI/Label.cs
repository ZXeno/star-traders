﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace StarTraders
{
    internal class Label : IControl
    {
        Game1 _game;

        public SpriteFont Font;
        public string Text = "";
        public Point Position;
        public bool Enabled { get; set; }
        public Rectangle AnchoredRect { get; set; }
        public Rectangle Bounds
        {
            get 
            { 
                return new Rectangle(
                    Position.X + AnchoredRect.X,
                    Position.Y + AnchoredRect.Y,
                    (int)Font.MeasureString(Text).X + 1,
                    (int)Font.MeasureString(Text).Y + 1
                    ); 
            }
        }
        

        public Label(Game1 game, SpriteFont font, string txt, Point position)
        {
            _game = game;
            Font = font;
            Text = txt;
            Position = position;
            AnchoredRect = new Rectangle(0, 0, _game.Graphics.PreferredBackBufferWidth, _game.Graphics.PreferredBackBufferHeight);
            Enabled = true;
        }

        public void Update(GameTime gameTime) 
        { 
            if(Enabled)
            {

            }
        }

        public void Draw()
        {
            if (Enabled)
            {
                _game.SpriteBatch.DrawString(Font, Text, new Vector2(Bounds.X, Bounds.Y), Color.White);
            }
        }

        public void HandleInput() { }

        public void SetPosition(Point p)
        {
            Position = p;
        }

        public bool HitTest() { return false; }
    }
}
