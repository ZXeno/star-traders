﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace StarTraders
{

    public enum ButtonTextPosition { Left, Center, Right }
    public enum GroupColumnPositioning { Left, Center, Right }

    internal class ListGroupRow : IControl
    {
        public List<IControl> RowItemsContainer { get; set; }

        public bool Enabled { get; set; }
        public Point Position { get; set; }
        public Rectangle AnchoredRect { get; set; }
        public int ColumnWidth { get; set; }
        public int RowHeight { get; set; }
        public int ColumnCount { get { return RowItemsContainer.Count; } }
        public Rectangle Bounds
        {
            get
            {
                return new Rectangle(
                    Position.X + AnchoredRect.X,
                    Position.Y + AnchoredRect.Y,
                    ColumnCount * ColumnWidth,
                    RowHeight
                    );
            }
        }

        public ListGroupRow(int columnwidth, int rowheight, params IControl[] rowmembers)
        {
            ColumnWidth = columnwidth;
            RowHeight = rowheight;
            RowItemsContainer = new List<IControl>();

            for(int i = 0; i < rowmembers.Length; i++)
            {
                RowItemsContainer.Add(rowmembers[i]);
                rowmembers[i].SetPosition(new Point(columnwidth * i, 0));
            }

            
        }

        public void Update(GameTime gameTime)
        {
            for (int i = 0; i < RowItemsContainer.Count; i++)
            {
                RowItemsContainer[i].AnchoredRect = this.Bounds;
                RowItemsContainer[i].Update(gameTime);
            }
        }

        public void Draw()
        {
            for (int i = 0; i < RowItemsContainer.Count; i++)
            {
                RowItemsContainer[i].Draw();
            }
        }

        public void HandleInput() { }

        public void SetPosition(Point p)
        {
            Position = p;
        }

        public bool HitTest()
        {
            if (Bounds.Contains(Input.MousePosition))
            {
                return true;
            }

            return false;
        }
    }

    internal class ListGroup : IControl
    {
        public Point Position;
        public int Width;
        public int Height;
        public int LineHeight = 20;
        public Rectangle AnchoredRect { get; set; }
        public List<IControl> Controls { get; protected set; }
        public ButtonTextPosition ButtonTextAlignment { get; set; }
        public bool Enabled { get; set; }
        public Rectangle Bounds
        {
            get 
            { 
                return new Rectangle(
                    Position.X + AnchoredRect.X, 
                    Position.Y + AnchoredRect.Y, 
                    Width, 
                    Height
                    ); 
            }
        }


        public ListGroup(Point position, int width)
        {
            Position = position;
            Width = width;
            Controls = new List<IControl>();
            ButtonTextAlignment = ButtonTextPosition.Center;
            Enabled = true;
        }

        public void Update(GameTime gameTime)
        {
            if (Enabled)
            {
                Height = LineHeight * Controls.Count;
                foreach (IControl c in Controls)
                {
                    c.AnchoredRect = this.Bounds;
                    c.Update(gameTime);
                }
            }
        }

        public void Draw()
        {
            if (Enabled)
            {
                foreach (IControl c in Controls)
                {
                    c.Draw();
                }
            }
        }

        public void AddControl(IControl control)
        {
            Controls.Add(control);
            Reposition();
        }

        public void Reposition()
        {
            foreach (IControl c in Controls)
            {
                c.AnchoredRect = this.Bounds;
                c.SetPosition(Point.Zero);
            }

            int index = 0;
            for (int i = 0; i < Controls.Count; i++)
            {
                IControl c = Controls[i];
                c.SetPosition(new Point(0, LineHeight * index));

                if(c is Button && ButtonTextAlignment == ButtonTextPosition.Right)
                {
                    Button b = c as Button;
                    b.Label.SetPosition(new Point(b.Position.X + b.Image.Width + 4, 0));
                }

                if (c is Button && ButtonTextAlignment == ButtonTextPosition.Center)
                {
                    Button b = c as Button;
                    b.Label.SetPosition(new Point(Convert.ToInt32(b.Position.X + (b.Image.Width / 2) - (b.Label.Font.MeasureString(b.Label.Text).X / 2)), b.Position.Y));
                }

                index++;
            }
        }

        public void Clear()
        {
            Controls.Clear();
        }

        public void SetPosition(Point p)
        {
            Position = p;
        }

        public bool HitTest()
        {
            if (Bounds.Contains(Input.MousePosition))
            {
                return true;
            }

            return false;
        }

        public void HandleInput() { }
    }
}
