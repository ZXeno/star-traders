﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace StarTraders
{
    public class Window : IControl
    {
        private Game1 _game;

        private Texture2D _bgImg;
        private Texture2D _closeButtonSprite;
        private Button _closeButton;
        private Label _titleLabel;
        private SpriteFont _font;
        private List<IControl> _container;
        public Rectangle AnchoredRect { get; set; }
        public Point Position { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int CornerSize { get; set; }
        public bool Enabled { get; set; }
        public bool CanDrag { get; set; }
        public bool IsHovered { get; private set; }
        
        public Rectangle Bounds
        {
            get
            {
                return new Rectangle(
                    Position.X + AnchoredRect.X,
                    Position.Y + AnchoredRect.Y,
                    Width,
                    Height
                    );
            }
        }

        public Window(Game1 game, Texture2D sprite, Texture2D closeButtonSprite, SpriteFont font, Point position, string windowtitle, int width, int height)
        {
            // Set window parameters
            Width = width;
            Height = height;
            CornerSize = 8;
            Position = position;
            Enabled = true;
            CanDrag = false;

            // Set window resources
            _bgImg = sprite;
            _font = font;
            _game = game;
            
            // Establish container and anchor
            _container = new List<IControl>();
            AnchoredRect = new Rectangle(0, 0, _game.Graphics.PreferredBackBufferWidth, _game.Graphics.PreferredBackBufferHeight);

            // Set window title
            string title = windowtitle;
            if (title == ""){title = "NEW_WINDOW";}
            _titleLabel = new Label(_game, _font, title, new Point(this.Width / 2 - Convert.ToInt32(_font.MeasureString(title).X) / 2, 2));

            // Set close button
            _closeButtonSprite = closeButtonSprite;
            _closeButton = new Button(_game, _closeButtonSprite, new Point(Width - (_closeButtonSprite.Width), 0), _font);
            _closeButton.D = new Action<Button>(
                delegate
                {
                    this.Enabled = false;
                });
            _closeButton.StretchBGImage = false;
            _closeButton.AnchoredRect = this.Bounds;

            // Add close button and title to window
            AddChildControl(_titleLabel);
            AddChildControl(_closeButton);
        }

        // Update the controls contained in this object
        public void Update(GameTime gameTime)
        {
            IsHovered = false;

            // if enabled
            if (Enabled)
            {
                HandleInput();

                if (_container.Count > 0)
                {
                    // update each control, assign anchor
                    foreach (IControl ctrl in _container)
                    {
                        ctrl.AnchoredRect = this.Bounds;
                        ctrl.Update(gameTime);
                    }
                }
            }
        }

        // Handle input if enabled
        public void HandleInput()
        {
            if (Enabled)
            {
                // handle dragging
                if (CanDrag && HitTest())
                {
                    if ((Input.GetButton(0) && Input.LastMouseState.LeftButton == ButtonState.Pressed))
                    {
                        if (Input.MousePosition != Input.LastMouseState.Position)
                        {
                            // set window position
                            Position = new Point(
                                Position.X + (Input.MousePosition.X - Input.LastMouseState.Position.X), 
                                Position.Y + (Input.MousePosition.Y - Input.LastMouseState.Position.Y)
                                );
                        }
                    }
                }

                // check for hover
                if (!IsHovered && HitTest())
                {
                    IsHovered = true;
                }
            }
        }

        public void Draw()
        {
            // If enabled
            if (Enabled)
            {
                // Draw the Window background according to the slice layout
                _game.DrawBox(_bgImg, Bounds, CornerSize, Color.White);

                if (_container.Count > 0)
                {
                    // Draw controls
                    foreach (IControl ctrl in _container)
                    {
                        ctrl.Draw();
                    }
                }
            }
        }

        // set the position of this object
        public void SetPosition(Point p)
        {
            Position = p;
        }

        // Add control, set anchor
        public void AddChildControl(IControl control)
        {
            _container.Add(control);
            control.AnchoredRect = this.Bounds;
        }

        // Remove control from this window
        public void RemoveChildControl(IControl control)
        {
            _container.Remove(control);
            control.AnchoredRect = new Rectangle(0, 0, _game.Graphics.PreferredBackBufferWidth, _game.Graphics.PreferredBackBufferHeight);
        }

        // set all controls in this window to "Enabled = true;"
        public void EnableAllControls()
        {
            foreach(IControl ctrl in _container)
            {
                ctrl.Enabled = true;
            }
        }

        // Set all controls in this window to "Enabled = false"
        public void DisableAllControls()
        {
            foreach (IControl ctrl in _container)
            {
                ctrl.Enabled = false;
            }
        }

        // Sets the window title
        public void SetWindowTitle(string newtitle)
        {
            _titleLabel.Text = newtitle;
        }

        public bool HitTest()
        {
            if (Bounds.Contains(Input.MousePosition))
            {
                return true;
            }

            return false;
        }
    }
}
